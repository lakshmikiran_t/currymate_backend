/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Map;

/**
 * @author LakshmiKiran
 *
 */
public class Mail {
	
	private String from;
    private String to;
    private String subject;
    private String content;
    private Map<String, ?> model;
    
    
	public Map<String, ?> getModel() {
		return model;
	}
	public void setModel(Map<String, ?> model) {
		this.model = model;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "Mail [from=" + from + ", to=" + to + ", subject=" + subject + ", content=" + content + ", model="
				+ model + "]";
	}
    
    
}
