/**
 * 
 */
package com.innoveyes.currymate.bean;

/**
 * @author LakshmiKiran
 *
 */
public class GroceryBean {
	
	private String groceryId;
	private String groceryName;
	private Integer quantity;
	
	public String getGroceryId() {
		return groceryId;
	}
	public void setGroceryId(String groceryId) {
		this.groceryId = groceryId;
	}
	public String getGroceryName() {
		return groceryName;
	}
	public void setGroceryName(String groceryName) {
		this.groceryName = groceryName;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "GroceryBean [groceryId=" + groceryId + ", groceryName=" + groceryName + ", quantity=" + quantity + "]";
	}
	
	
}
