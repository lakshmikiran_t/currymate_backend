/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author LakshmiKiran
 *
 */
public class UserProfileBean {
	
	private String userId;
	private String firstName;
	private String lastName;
	private String mobile;
	private String email;
	private String password;
	private String locationId;
	private String areaId;
	private String roleId;
	private String sessionToken;
	@JsonIgnore
	private Boolean isVerified = false;
	private String otp;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	@JsonIgnore
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	private Date createdDate;
	@JsonIgnore
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss", timezone="IST")
	private Date updatedDate;
	@JsonIgnore
	private Boolean isLoggedOut ;
	
	
	public Boolean getIsLoggedout() {
		return isLoggedOut;
	}
	public void setIsLoggedout(Boolean isLoggedout) {
		this.isLoggedOut = isLoggedout;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getSessionToken() {
		return sessionToken;
	}
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}
	public Boolean getIsVerified() {
		return isVerified;
	}
	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}
	@Override
	public String toString() {
		return "UserProfileBean [userId=" + userId + ", firstName=" + firstName + ", lastName=" + lastName + ", mobile="
				+ mobile + ", email=" + email + ", password=" + password + ", locationId=" + locationId + ", areaId="
				+ areaId + ", roleId=" + roleId + ", sessionToken=" + sessionToken + ", isVerified=" + isVerified
				+ ", otp=" + otp + ", statusCode=" + statusCode + ", statusMessage=" + statusMessage + ", createdDate="
				+ createdDate + ", updatedDate=" + updatedDate + ", isLoggedout=" + isLoggedOut + "]";
	}
	public String getPassword() {
		return password;
	}
	public String getOtp() {
		return otp;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	
}
