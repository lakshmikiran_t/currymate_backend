/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Date;

/**
 * @author LakshmiKiran
 *
 */
public class VegetableBean {
	
	private String vegetableId;
	private String vegetableName;
	private Integer costPerKG;
	private Integer quantity;
	private Integer totalVegetableCost;
	private Date createdDate;
	
	public String getVegetableId() {
		return vegetableId;
	}
	public void setVegetableId(String vegetableId) {
		this.vegetableId = vegetableId;
	}
	public String getVegetableName() {
		return vegetableName;
	}
	public void setVegetableName(String vegetableName) {
		this.vegetableName = vegetableName;
	}
	public Integer getCostPerKG() {
		return costPerKG;
	}
	public void setCostPerKG(Integer costPerKG) {
		this.costPerKG = costPerKG;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getTotalVegetableCost() {
		return totalVegetableCost;
	}
	public void setTotalVegetableCost(Integer totalVegetableCost) {
		this.totalVegetableCost = totalVegetableCost;
	}
	public void setTotalVegetableCost() {
		this.totalVegetableCost = this.costPerKG * this.quantity;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "VegetableBean [vegetableId=" + vegetableId + ", vegetableName=" + vegetableName + ", costPerKG="
				+ costPerKG + ", quantity=" + quantity + ", totalCost=" + totalVegetableCost + ", createdDate=" + createdDate
				+ "]";
	}
	
	
}
