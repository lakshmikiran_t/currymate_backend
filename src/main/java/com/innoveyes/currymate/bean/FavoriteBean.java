package com.innoveyes.currymate.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Lakshmi Kiran
 *
 */
public class FavoriteBean {
		
	private String favoriteId;
	private String userProfileId;
	private String storeId;
	private Date createdDate;
	@JsonIgnore
	private List<FavoriteBean> beans;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	public String getFavoriteId() {
		return favoriteId;
	}
	public void setFavoriteId(String favoriteId) {
		this.favoriteId = favoriteId;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userId) {
		this.userProfileId = userId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public List<FavoriteBean> getBeans() {
		return beans;
	}
	public void setBeans(List<FavoriteBean> beans) {
		this.beans = beans;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Override
	public String toString() {
		return "FavoriteBean [favoriteId=" + favoriteId + ", userId=" + userProfileId + ", storeId=" + storeId
				+ ", createdDate=" + createdDate + ", beans=" + beans + ", statusCode=" + statusCode
				+ ", statusMessage=" + statusMessage + "]";
	}
	
	
}
