/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Lakshmi Kiran
 *
 */
public class RatingBean {
	
	private String ratingId;
	private String userProfileId;
	private String storeId;
	private Integer rating;
	private Date createdDate;
	@JsonIgnore
	private List<RatingBean> beans;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	
	public String getRatingId() {
		return ratingId;
	}
	public void setRatingId(String ratingId) {
		this.ratingId = ratingId;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public List<RatingBean> getBeans() {
		return beans;
	}
	public void setBeans(List<RatingBean> beans) {
		this.beans = beans;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Override
	public String toString() {
		return "RatingBean [ratingId=" + ratingId + ", userProfileId=" + userProfileId + ", storeId=" + storeId
				+ ", rating=" + rating + ", createdDate=" + createdDate + ", beans=" + beans + ", statusCode="
				+ statusCode + ", statusMessage=" + statusMessage + "]";
	}
	
	
}
