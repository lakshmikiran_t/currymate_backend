/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author LakshmiKiran
 *
 */
public class CartBean {
	
	private String cartId;
	private List<OrderBean> orders;
	private Integer checkOutCost;
	private Boolean isPaid;
	private Date cartedDate;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	@JsonIgnore
	private List<CartBean> beans;
	
	
	public List<CartBean> getBeans() {
		return beans;
	}
	public void setBeans(List<CartBean> beans) {
		this.beans = beans;
	}
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public List<OrderBean> getOrders() {
		return orders;
	}
	public void setOrders(List<OrderBean> orders) {
		this.orders = orders;
	}
	public Integer getCheckOutCost() {
		return checkOutCost;
	}
	public void setCheckOutCost(Integer checkOutCost) {
		this.checkOutCost = checkOutCost;
	}
	public Boolean getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}
	public Date getCartedDate() {
		return cartedDate;
	}
	public void setCartedDate(Date cartedDate) {
		this.cartedDate = cartedDate;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Override
	public String toString() {
		return "CartBean [cartId=" + cartId + ", orders=" + orders + ", checkOutCost=" + checkOutCost + ", isPaid="
				+ isPaid + ", cartedDate=" + cartedDate + ", statusCode=" + statusCode + ", statusMessage="
				+ statusMessage + "]";
	}
	
	
}
