/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.innoveyes.currymate.model.master.Area;
import com.innoveyes.currymate.model.master.Grocery;
import com.innoveyes.currymate.model.master.Item;
import com.innoveyes.currymate.model.master.Location;
import com.innoveyes.currymate.model.master.UserRole;
import com.innoveyes.currymate.model.master.Vegetable;

/**
 * @author LakshmiKiran
 *
 */
public class MasterDataBean {
	
	private List<Area> areas;
	private List<Location> locations;
	private List<UserRole> userRoles;
	private List<Vegetable> vegetables;
	private List<Item> items;
	private List<Grocery> grocerys;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	public List<Area> getAreas() {
		return areas;
	}
	public void setAreas(List<Area> areas) {
		this.areas = areas;
	}
	public List<Location> getLocations() {
		return locations;
	}
	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}
	public List<UserRole> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}
	public List<Vegetable> getVegetables() {
		return vegetables;
	}
	public void setVegetables(List<Vegetable> vegetables) {
		this.vegetables = vegetables;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	public List<Grocery> getGrocerys() {
		return grocerys;
	}
	public void setGrocerys(List<Grocery> grocerys) {
		this.grocerys = grocerys;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Override
	public String toString() {
		return "MasterDataBean [areas=" + areas + ", locations=" + locations + ", userRoles=" + userRoles
				+ ", vegetables=" + vegetables + ", items=" + items + ", grocerys=" + grocerys + ", statusCode="
				+ statusCode + ", statusMessage=" + statusMessage + "]";
	}
	

}
