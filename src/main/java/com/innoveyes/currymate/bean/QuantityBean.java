/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.innoveyes.currymate.model.GroceryQuantity;
import com.innoveyes.currymate.model.VegetableQuantity;

/**
 * @author LakshmiKiran
 *
 */
public class QuantityBean {
	
	private String groceryQuantityId;
	private String groceryId;
	private String storeId;
	private String groceryName;
	private String vegetableQuantityId;
	private String vegetableId;
	private String vegetableName;
	private Integer costPerKG;
	private Integer totalQuantity;
	private Integer usedQuantity;
	private Integer remainingQuantity;
	private Integer totalGroceryCost;
	private Integer totalVegetableCost;
	private Boolean isFinished = false;
	@JsonIgnore
	private List<VegetableQuantity> vegetables;
	@JsonIgnore
	private List<GroceryQuantity> groceries;
	private Date createdDate;
	private Date updatedDate;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	
	

	public Integer getTotalVegetableCost() {
		return totalVegetableCost;
	}
	public void setTotalVegetableCost(Integer totalVegetableCost) {
		this.totalVegetableCost = totalVegetableCost;
	}
	public List<VegetableQuantity> getVegetables() {
		return vegetables;
	}
	public void setVegetables(List<VegetableQuantity> vegetables) {
		this.vegetables = vegetables;
	}
	public List<GroceryQuantity> getGroceries() {
		return groceries;
	}
	public void setGroceries(List<GroceryQuantity> groceries) {
		this.groceries = groceries;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getGroceryQuantityId() {
		return groceryQuantityId;
	}
	public void setGroceryQuantityId(String groceryQuantityId) {
		this.groceryQuantityId = groceryQuantityId;
	}
	public String getGroceryId() {
		return groceryId;
	}
	public void setGroceryId(String groceryId) {
		this.groceryId = groceryId;
	}
	public String getGroceryName() {
		return groceryName;
	}
	public void setGroceryName(String groceryName) {
		this.groceryName = groceryName;
	}
	public String getVegetableQuantityId() {
		return vegetableQuantityId;
	}
	public void setVegetableQuantityId(String vegetableQuantityId) {
		this.vegetableQuantityId = vegetableQuantityId;
	}
	public String getVegetableId() {
		return vegetableId;
	}
	public void setVegetableId(String vegetableId) {
		this.vegetableId = vegetableId;
	}
	public String getVegetableName() {
		return vegetableName;
	}
	public void setVegetableName(String vegetableName) {
		this.vegetableName = vegetableName;
	}
	public Integer getCostPerKG() {
		return costPerKG;
	}
	public void setCostPerKG(Integer costPerKG) {
		this.costPerKG = costPerKG;
	}
	public Integer getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public Integer getUsedQuantity() {
		return usedQuantity;
	}
	public void setUsedQuantity(Integer usedQuantity) {
		this.usedQuantity = usedQuantity;
	}
	public Integer getRemainingQuantity() {
		return remainingQuantity;
	}
	public void setRemainingQuantity(Integer remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}
	public Integer getTotalGroceryCost() {
		return totalGroceryCost;
	}
	public void setTotalGroceryCost(Integer totalGroceryCost) {
		this.totalGroceryCost = totalGroceryCost;
	}
	public Boolean getIsFinished() {
		return isFinished;
	}
	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "QuantityBean [groceryQuantityId=" + groceryQuantityId + ", groceryId=" + groceryId + ", storeId="
				+ storeId + ", groceryName=" + groceryName + ", vegetableQuantityId=" + vegetableQuantityId
				+ ", vegetableId=" + vegetableId + ", vegetableName=" + vegetableName + ", costPerKG=" + costPerKG
				+ ", totalQuantity=" + totalQuantity + ", usedQuantity=" + usedQuantity + ", remainingQuantity="
				+ remainingQuantity + ", totalGroceryCost=" + totalGroceryCost + ", totalVegetableCost="
				+ totalVegetableCost + ", isFinished=" + isFinished + ", vegetables=" + vegetables + ", groceries="
				+ groceries + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + ", statusCode="
				+ statusCode + ", statusMessage=" + statusMessage + "]";
	}
	
	
}
