/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author LakshmiKiran
 *
 */
public class OrderBean {
	
	private String orderId;
	private String preparedItemId;
	private String storeId;
	private String userProfileId;
	private Integer noOfPacks;
	private Integer orderCost;
	private Integer itemCost;
	private Boolean isCancelled = false;
	private Boolean isPaymentDone = false;
	private Date orderedDate;
	@JsonIgnore
	private List<OrderBean> beans;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	
	
	public List<OrderBean> getBeans() {
		return beans;
	}
	public void setBeans(List<OrderBean> beans) {
		this.beans = beans;
	}
	public Boolean getIsPaymentDone() {
		return isPaymentDone;
	}
	public void setIsPaymentDone(Boolean isPaymentDone) {
		this.isPaymentDone = isPaymentDone;
	}
	public Boolean getIsCancelled() {
		return isCancelled;
	}
	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPreparedItemId() {
		return preparedItemId;
	}
	public void setPreparedItemId(String preparedItemId) {
		this.preparedItemId = preparedItemId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}
	public Integer getNoOfPacks() {
		return noOfPacks;
	}
	public void setNoOfPacks(Integer noOfPacks) {
		this.noOfPacks = noOfPacks;
	}
	public Integer getOrderCost() {
		return orderCost;
	}
	public void setOrderCost(Integer orderCost) {
		this.orderCost = orderCost;
	}
	public Integer getItemCost() {
		return itemCost;
	}
	public void setItemCost(Integer itemCost) {
		this.itemCost = itemCost;
	}
	public Date getOrderedDate() {
		return orderedDate;
	}
	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	@Override
	public String toString() {
		return "OrderBean [orderId=" + orderId + ", preparedItemId=" + preparedItemId + ", storeId=" + storeId
				+ ", userProfileId=" + userProfileId + ", noOfPacks=" + noOfPacks + ", orderCost=" + orderCost
				+ ", itemCost=" + itemCost + ", isCancelled=" + isCancelled + ", isPaymentDone=" + isPaymentDone
				+ ", orderedDate=" + orderedDate + ", beans=" + beans + ", statusCode=" + statusCode
				+ ", statusMessage=" + statusMessage + "]";
	}
}
