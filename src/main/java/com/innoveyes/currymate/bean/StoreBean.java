/**
 * 
 */
package com.innoveyes.currymate.bean;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author LakshmiKiran
 *
 */
public class StoreBean {
	

	private String stroreId;
	private String storeName;
	private String userProfileId;
	private String locationId;
	private String areaId;
	private String storeAddress;
	private String storePhone;
	private String pincode;
	private String[] storeImages;
	private Integer rating;
	private Boolean isFavorite;
	private Date createdDate;
	private Date updatedDate;
	@JsonIgnore
	private Integer statusCode;
	@JsonIgnore
	private String statusMessage;
	@JsonIgnore
	private List<StoreBean> stores;
	
	
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public Boolean getIsFavorite() {
		return isFavorite;
	}
	public void setIsFavorite(Boolean isFavourite) {
		this.isFavorite = isFavourite;
	}
	public List<StoreBean> getStores() {
		return stores;
	}
	public void setStores(List<StoreBean> stores) {
		this.stores = stores;
	}
	public Integer getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public String getStroreId() {
		return stroreId;
	}
	public void setStroreId(String stroreId) {
		this.stroreId = stroreId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}
	public String getStorePhone() {
		return storePhone;
	}
	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String[] getStoreImages() {
		return storeImages;
	}
	public void setStoreImages(String[] storeImages) {
		this.storeImages = storeImages;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "StoreBean [stroreId=" + stroreId + ", storeName=" + storeName + ", userProfileId=" + userProfileId + ", locationId="
				+ locationId + ", areaId=" + areaId + ", storeAddress=" + storeAddress + ", storePhone=" + storePhone
				+ ", pincode=" + pincode + ", storeImages=" + Arrays.toString(storeImages) + ", rating=" + rating
				+ ", isFavourite=" + isFavorite + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate
				+ ", statusCode=" + statusCode + ", statusMessage=" + statusMessage + ", stores=" + stores + "]";
	}

	
}
