/**
 * 
 */
package com.innoveyes.currymate.service;

import com.innoveyes.currymate.bean.QuantityBean;

/**
 * @author LakshmiKiran
 *
 */
public interface QuantityService {

	public QuantityBean saveGroceryQuantity(QuantityBean bean);

	public QuantityBean saveVegetableQuantuty(QuantityBean bean);

	public QuantityBean getGroceryQuantityByStore(String groceryId, String storeId);

	public QuantityBean getVegetableQuantityByStore(String vegetableId, String storeId);

	public QuantityBean getGroceriesForStore(String storeId);

	public QuantityBean getVegetablesForStore(String storeId);

	public QuantityBean getFinishedVegQuantityForStore(String storeId);

	public QuantityBean getFinishedGroceryByStore(String storeId);
}
