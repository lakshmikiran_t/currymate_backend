/**
 * 
 */
package com.innoveyes.currymate.service;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.RatingBean;
import com.innoveyes.currymate.bean.UserProfileBean;
import com.innoveyes.currymate.dao.RatingAndFavoriteDao;
import com.innoveyes.currymate.dao.UserProfileDao;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.model.Rating;
import com.innoveyes.currymate.model.UserLoginStatus;
import com.innoveyes.currymate.model.UserProfile;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class UserProfileServiceImpl implements UserProfileService {

	private Logger logger = LogManager.getLogger(UserProfileServiceImpl.class);

	@Autowired
	private UserProfileDao userProfileDao;

	@Autowired
	private RatingAndFavoriteDao ratingAndFavoriteDao;

	@Override
	public String isUserLoggedIn(String userId) {

		return null;
	}

	@Override
	public UserProfileBean signUp(UserProfileBean bean) throws Exception {

		try {
			if (bean.getRoleId().equals(Message.ROLE_ADMIN.value())) {
				bean.setIsVerified(true);
				UserProfile user = getUserProfile(bean);
				user.setIsVerified(true);
				user = userProfileDao.signUp(user);
				bean = getUserProfileBean(user);
				bean.setStatusCode(1);
				bean.setStatusMessage("Admin profile created successfully");
				return bean;
			}
			bean.setOtp(sendSMS(bean.getMobile()));
			UserProfile user = userProfileDao.signUp(getUserProfile(bean));
			bean = getUserProfileBean(user);
			bean.setStatusCode(1);
			bean.setStatusMessage("User profile created successfully");
			return bean;
		} catch (DuplicateKeyException e) {
			logger.error("problem in sign up process", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("An account with mobile no : " + bean.getMobile() + " already exists");
			return bean;
		}
	}

	private UserProfile getUserProfile(UserProfileBean bean) throws Exception {
		UserProfile user = new UserProfile();
		user.setAreaId(bean.getAreaId());
		user.setCreatedDate(new Date());
		user.setEmail(bean.getEmail());
		user.setFirstName(bean.getFirstName());
		user.setIsVerified(false);
		user.setLastName(bean.getLastName());
		user.setLocationId(bean.getLocationId());
		user.setMobile(bean.getMobile());
		user.setOtp(bean.getOtp());
		user.setPassword(encrypt(bean.getPassword()));
		user.setUpdatedDate(new Date());
		user.setUserRoleId(bean.getRoleId());

		return user;
	}

	private UserProfileBean getUserProfileBean(UserProfile bean) throws Exception {
		UserProfileBean user = new UserProfileBean();
		user.setAreaId(bean.getAreaId());
		user.setCreatedDate(new Date());
		user.setEmail(bean.getEmail());
		user.setFirstName(bean.getFirstName());
		user.setIsVerified(bean.getIsVerified());
		user.setLastName(bean.getLastName());
		user.setLocationId(bean.getLocationId());
		user.setMobile(bean.getMobile());
		user.setUpdatedDate(new Date());
		user.setRoleId(bean.getUserRoleId());
		user.setUserId(bean.getUserId());
		user.setOtp(bean.getOtp());

		return user;
	}

	private static String byteArrayToHexString(byte[] b) {
		StringBuffer sb = new StringBuffer(b.length * 2);
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xff;
			if (v < 16) {
				sb.append('0');
			}
			sb.append(Integer.toHexString(v));
		}
		return sb.toString().toUpperCase();
	}

	private static byte[] hexStringToByteArray(String s) {
		byte[] b = new byte[s.length() / 2];
		for (int i = 0; i < b.length; i++) {
			int index = i * 2;
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			b[i] = (byte) v;
		}
		return b;
	}

	private String encrypt(String password) throws InvalidKeyException, InvalidAlgorithmParameterException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

		byte[] bytekey = hexStringToByteArray(Message.SECRET_KEY.value());
		SecretKeySpec sks = new SecretKeySpec(bytekey, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, sks, cipher.getParameters());
		byte[] encrypted = cipher.doFinal(password.getBytes());
		return byteArrayToHexString(encrypted);
	}

	private String decrypt(String password) throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		byte[] bytekey = hexStringToByteArray(Message.SECRET_KEY.value());
		SecretKeySpec sks = new SecretKeySpec(bytekey, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, sks);
		byte[] decrypted = cipher.doFinal(hexStringToByteArray(password));
		return new String(decrypted);
	}

	@Override
	public UserProfileBean login(UserProfileBean bean) throws Exception {
		try {
			UserProfile user = userProfileDao.findUserByMobile(bean.getMobile());
			if (user == null) {
				logger.info("No user registered with mobile no : " + bean.getMobile());
				bean.setStatusCode(0);
				bean.setStatusMessage("Not user registered with mobile no : " + bean.getMobile());
				return bean;
			}
			if (!user.getIsVerified()) {
				logger.info("Mobile verification is not done");
				bean.setStatusCode(0);
				bean.setStatusMessage("Please verify your mobile and continue to login");
				return bean;
			}
			if (bean.getPassword().equals(decrypt(user.getPassword()))) {
				logger.info("User logged in successfully");
				String sessionToken = generateRandomString();
				user.setUpdatedDate(new Date());
				userProfileDao.updateUser(user);
				logger.info("Session Created for the user");
				bean = getUserProfileBean(user);
				bean.setStatusCode(1);
				bean.setStatusMessage("User logged in successfully");
				bean.setSessionToken(sessionToken);
				UserLoginStatus status = userProfileDao.findLoginStatusById(user.getUserId());
				if (status != null) {
					status.setIsLoggedOut(true);
					status.setLastLoggedIn(status.getUpdatedDate());
					status.setUpdatedDate(new Date());
					userProfileDao.saveUserLoginStatus(status);
				}
				status = new UserLoginStatus();
				status.setCreatedDate(new Date());
				status.setIsLoggedOut(false);
				status.setLastLoggedIn(new Date());
				status.setSessionToken(sessionToken);
				status.setUserId(user.getUserId());
				status.setUserRoleId(user.getUserRoleId());
				status.setUpdatedDate(new Date());
				userProfileDao.saveUserLoginStatus(status);
				return bean;
			}
			logger.info("Incorrect Password, please login using apt password");
			bean.setStatusCode(0);
			bean.setStatusMessage("Incorrect Password, please login using apt password");
			return bean;
		} catch (Exception e) {
			logger.error("Problem in the login process", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("Exception");
			return bean;
		}
	}

	@Override
	public String sendSMS(String mobile) {
		// TODO Auto-generated method stub
		return "123456";
	}

	@Override
	public UserProfileBean verifyUser(String mobile, String otp) {
		UserProfileBean bean = new UserProfileBean();
		try {
			UserProfile user = userProfileDao.findUserByMobile(mobile);
			if (user == null) {
				logger.info("No user registered with mobile no : " + bean.getMobile());
				bean.setStatusCode(0);
				bean.setStatusMessage("No user registered with mobile no : " + bean.getMobile());
				return bean;
			}
			if (otp.equals(user.getOtp())) {
				logger.info("User verified successfully");
				user.setIsVerified(true);
				userProfileDao.updateUser(user);
				bean = getUserProfileBean(user);
				bean.setStatusCode(1);
				bean.setStatusMessage("User verified successfully");
				return bean;
			}
			logger.info("Invalid OTP");
			bean.setStatusCode(0);
			bean.setStatusMessage("Invalid OTP, please enter valid otp");
			return bean;
		} catch (Exception e) {
			logger.error("Exception while verifying user");
			bean.setStatusCode(2);
			bean.setStatusMessage("Exception while verifying user");
			return bean;
		}
	}

	public String generateRandomString() {

		StringBuffer randStr = new StringBuffer();
		for (int i = 0; i < 24; i++) {
			int number = getRandomNumber();
			char ch = Message.CHAR_LIST.value().charAt(number);
			randStr.append(ch);
		}
		return randStr.toString();
	}

	private int getRandomNumber() {
		int randomInt = 0;
		Random randomGenerator = new Random();
		randomInt = randomGenerator.nextInt(Message.CHAR_LIST.value().length());
		if (randomInt - 1 == -1) {
			return randomInt;
		} else {
			return randomInt - 1;
		}
	}

	@Override
	public UserProfileBean findUserLoginStatus(String sessionToken) {
		UserProfileBean bean = new UserProfileBean();
		UserLoginStatus status = userProfileDao.findUserLoginStatus(sessionToken);
		if (status == null) {
			logger.info("Not logged in yet");
			bean.setStatusCode(0);
			bean.setStatusMessage("please login to continue");
			return bean;
		}
		long difference = new Date().getTime() - status.getUpdatedDate().getTime();
		long mins = difference / (60 * 1000);
		logger.info("last used " + mins + " mins ago");
		if (mins >= 30) {
			logger.info("session expired");
			bean.setStatusCode(0);
			bean.setStatusMessage("session expired, please login to continue");
			status.setIsLoggedOut(true);
			userProfileDao.saveUserLoginStatus(status);
			return bean;
		}
		logger.info("session active");
		bean.setStatusCode(1);
		bean.setStatusMessage("session active, please continue");
		bean.setUserId(status.getUserId());
		bean.setSessionToken(status.getSessionToken());
		bean.setRoleId(status.getUserRoleId());
		status.setUpdatedDate(new Date());
		status.setLastLoggedIn(new Date());
		userProfileDao.saveUserLoginStatus(status);
		return bean;
	}

	@Override
	public RatingBean rateStore(RatingBean bean) {
		try {
			Rating rating = new Rating();
			rating.setRating(bean.getRating());
			rating.setRatingId(bean.getRatingId());
			rating.setCreatedDate(new Date());
			rating.setStoreId(bean.getStoreId());
			rating.setUserProfileId(bean.getUserProfileId());
			ratingAndFavoriteDao.rateStore(rating);
			bean.setStatusCode(1);
			bean.setStatusMessage("Rated a store successfully");
			logger.info("Rated a store successfully");
			return bean;
		} catch (Exception e) {
			logger.error("Exception while rating a store", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("Exception while rating a store");
			return bean;
		}
	}
}
