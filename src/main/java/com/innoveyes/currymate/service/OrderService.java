/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.Date;

import com.innoveyes.currymate.bean.OrderBean;
import com.innoveyes.currymate.model.Order;

/**
 * @author LakshmiKiran
 *
 */
public interface OrderService {

	public OrderBean saveOrder(OrderBean bean);

	public OrderBean findOrderByUserStoreAndDate(String userId, String storeId, Date orderedDate);

	public OrderBean findOrderByUserStore(String userId, String storeId);

	public OrderBean findOrderByUserStoreAndCancelled(String userId, String storeId);

	public Order getOrder(OrderBean bean);
	
	public OrderBean getOrderBean(Order bean);
}
