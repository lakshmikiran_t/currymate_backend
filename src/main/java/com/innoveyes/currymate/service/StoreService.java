/**
 * 
 */
package com.innoveyes.currymate.service;

import com.innoveyes.currymate.bean.FavoriteBean;
import com.innoveyes.currymate.bean.StoreBean;

/**
 * @author LakshmiKiran
 *
 */
public interface StoreService {

	public StoreBean saveStore(StoreBean bean);

	public StoreBean findStoreByPhone(String storePhone);

	public StoreBean findStoresByLocation(String locationId);

	public StoreBean findStoresByArea(String areaId);

	public StoreBean findStoresForUser(String userId);

	public void sendSMS(String mobile);
	
	public Integer findStoreRating(String userProfileId, String storeId);
	
	public FavoriteBean addFavorite(String userProfileId, String storeId);
	
	public FavoriteBean deleteFavorite(String userProfileId, String storeId);
	
	public StoreBean getFavoriteStores(String userProfileId);
}
