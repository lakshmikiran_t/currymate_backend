/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.GroceryBean;
import com.innoveyes.currymate.bean.PreparedItemBean;
import com.innoveyes.currymate.bean.VegetableBean;
import com.innoveyes.currymate.dao.GroceryQuantityDao;
import com.innoveyes.currymate.dao.PreparedItemDao;
import com.innoveyes.currymate.dao.VegetableQuantityDao;
import com.innoveyes.currymate.model.GroceryQuantity;
import com.innoveyes.currymate.model.PreparedItem;
import com.innoveyes.currymate.model.VegetableQuantity;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class PreparedItemServiceImpl implements PreparedItemService {

	private Logger logger = LogManager.getLogger(PreparedItemServiceImpl.class);

	@Autowired
	private PreparedItemDao preparedItemDao;

	@Autowired
	private GroceryQuantityDao groceryQuantityDao;

	@Autowired
	private VegetableQuantityDao vegetableQuantityDao;

	@Override
	public PreparedItemBean savePreparedItem(List<PreparedItemBean> beans) {
		logger.info("creating a prepared item");
		int totalGroceryCost = 0;
		int totalVegetableCost = 0;
		PreparedItemBean itemBean = new PreparedItemBean();
		try {
			for (PreparedItemBean bean : beans) {
				logger.info("checking and updating grocery quantities");
				for (GroceryBean gBean : bean.getGroceries()) {
					GroceryQuantity quantity = groceryQuantityDao.getGroceryQuantityByStore(gBean.getGroceryId(),
							bean.getStoreId());
					quantity.setUsedQuantity(quantity.getUsedQuantity() + gBean.getQuantity());
					quantity.setRemainingQuantity(
							quantity.getTotalQuantity() - (quantity.getUsedQuantity() + gBean.getQuantity()));
					if (quantity.getTotalQuantity() == (quantity.getUsedQuantity() + gBean.getQuantity())) {
						quantity.setIsFinished(true);
					}
					totalGroceryCost = totalGroceryCost + quantity.getTotalGroceryCost();
					groceryQuantityDao.saveGroceryQuantity(quantity);
				}
				logger.info("checking and updating vegetable quantities");
				for (VegetableBean vBean : bean.getVegetables()) {
					VegetableQuantity quantity = vegetableQuantityDao
							.getVegetableQuantityByStore(vBean.getVegetableId(), bean.getStoreId());
					quantity.setUsedQuantity(quantity.getUsedQuantity() + vBean.getQuantity());
					quantity.setRemainingQuantity(
							quantity.getTotalQuantity() - (quantity.getUsedQuantity() + vBean.getQuantity()));
					if (quantity.getTotalQuantity() == (quantity.getUsedQuantity() + vBean.getQuantity())) {
						quantity.setIsFinished(true);
					}
					totalVegetableCost = totalGroceryCost + quantity.getTotalVegetableCost();
					vegetableQuantityDao.saveVegetableQuantity(quantity);
				}
				PreparedItem item = getPreparedItem(bean);
				item.setTotalGroceryCost(totalGroceryCost);
				item.setTotalVegetableCost(totalVegetableCost);
				preparedItemDao.savePreparedItem(item);
			}
			logger.info("prepared items created successfully");
			itemBean.setStatusCode(1);
			itemBean.setStatusMessage("prepared items created successfully");
			return itemBean;
		} catch (Exception e) {
			logger.error("exception while saving prepared items");
			itemBean.setStatusCode(2);
			itemBean.setStatusMessage("exception while saving prepared items");
			return itemBean;
		}
	}

	@Override
	public PreparedItemBean getItemsByStoreId(String storeId, Date createdDate) {
		List<PreparedItemBean> beans = new ArrayList<>();
		PreparedItemBean bean = new PreparedItemBean();
		logger.info("retrieving prepared items for store : " + storeId);
		try {
			List<PreparedItem> items = new ArrayList<>();
			if (createdDate == null) {
				items = preparedItemDao.getItemsByStore(storeId);
			} else {
				items = preparedItemDao.getItemsByStoreAndDate(storeId, createdDate);
			}
			if (items == null || items.isEmpty()) {
				logger.info("no prepared items for the store : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no prepared items for the store : " + storeId);
				return bean;
			}
			for (PreparedItem item : items) {
				bean = getPreparedItemBean(item);
				beans.add(bean);
				bean = new PreparedItemBean();
			}
			bean = new PreparedItemBean();
			bean.setBeans(beans);
			bean.setStatusCode(1);
			logger.info("prepared items for the store : " + storeId + " retrieved successfully");
			bean.setStatusMessage("prepared items for the store : " + storeId + " retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving prepared items for the store : " + storeId);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving prepared items for the store : " + storeId);
			return bean;
		}
	}

	private PreparedItem getPreparedItem(PreparedItemBean bean) {
		PreparedItem item = new PreparedItem();
		item.setAdditionalCharges(bean.getAdditionalCharges());
		item.setGroceries(bean.getGroceries());
		item.setVegetables(bean.getVegetables());
		item.setItemId(bean.getItemId());
		item.setItemImage(bean.getItemImage());
		item.setItemType(bean.getItemType());
		item.setPreparedDate(new Date());
		item.setPreparedItemId(bean.getPreparedItemId());
		item.setPreparedItemName(bean.getPreparedItemName());
		item.setStoreId(bean.getStoreId());
		item.setTotalQuantity(bean.getTotalQuantity());
		item.setTotalPacks(bean.getTotalQuantity() / bean.getPacksPerKG());
		item.setUpdatedDate(new Date());
		item.setVegetables(bean.getVegetables());
		item.setCostPerPack(bean.getCostPerPack());
		item.setPacksPerKG(bean.getPacksPerKG());
		return item;
	}

	private PreparedItemBean getPreparedItemBean(PreparedItem item) {
		PreparedItemBean bean = new PreparedItemBean();
		bean.setAdditionalCharges(item.getAdditionalCharges());
		bean.setGroceries(item.getGroceries());
		bean.setCostPerPack(item.getCostPerPack());
		bean.setItemId(item.getItemId());
		bean.setItemImage(item.getItemImage());
		bean.setItemType(item.getItemType());
		bean.setOrderedPacks(item.getOrderedPacks());
		bean.setPacksPerKG(item.getPacksPerKG());
		bean.setPreparedDate(item.getPreparedDate());
		bean.setPreparedItemId(item.getPreparedItemId());
		bean.setPreparedItemName(bean.getPreparedItemName());
		bean.setRemainingPacks(item.getRemainingPacks());
		bean.setStoreId(item.getStoreId());
		bean.setTotalPacks(item.getTotalPacks());
		bean.setTotalQuantity(item.getTotalQuantity());
		bean.setVegetables(item.getVegetables());
		bean.setTotalCost(item.getTotalCost());
		bean.setTotalGroceryCost(item.getTotalGroceryCost());
		bean.setTotalVegetableCost(item.getTotalVegetableCost());
		return bean;
	}
}
