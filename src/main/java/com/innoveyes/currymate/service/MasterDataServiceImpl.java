/**
 * 
 */
package com.innoveyes.currymate.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.MasterDataBean;
import com.innoveyes.currymate.dao.MasterDataDao;
import com.innoveyes.currymate.model.master.Area;
import com.innoveyes.currymate.model.master.Grocery;
import com.innoveyes.currymate.model.master.Item;
import com.innoveyes.currymate.model.master.Location;
import com.innoveyes.currymate.model.master.UserRole;
import com.innoveyes.currymate.model.master.Vegetable;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class MasterDataServiceImpl implements MasterDataService {

	private Logger logger = LogManager.getLogger();

	@Autowired
	private MasterDataDao masterDataDao;

	@Override
	public MasterDataBean masterData() {

		logger.info("master data service");
		MasterDataBean bean = new MasterDataBean();
		try {
			bean.setAreas(masterDataDao.allAreas());
			bean.setGrocerys(masterDataDao.allGroceries());
			bean.setItems(masterDataDao.allItems());
			bean.setLocations(masterDataDao.allLocations());
			bean.setUserRoles(masterDataDao.allRoles());
			bean.setVegetables(masterDataDao.allVegetable());
			bean.setStatusCode(1);
			bean.setStatusMessage("master data retrieved succesfully");
			logger.info("master data retrieved succesfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving master data");
			bean.setStatusCode(0);
			bean.setStatusMessage("exception while retrieving master data");
			return bean;
		}
	}

	@Override
	public MasterDataBean saveMasterData(MasterDataBean bean) {

		if (bean.getAreas() != null) {
			for (Area area : bean.getAreas()) {
				try {
					masterDataDao.addArea(area);
				} catch (DuplicateKeyException e) {
					logger.error("Duplicate key in areas : " + area.getAreaName());
				}
			}
		}
		if (bean.getGrocerys() != null) {
			for (Grocery grocery : bean.getGrocerys()) {
				try {
					masterDataDao.addGrocery(grocery);
				} catch (DuplicateKeyException e) {
					logger.error("Duplicate key in groceries : " + grocery.getGroceryName());
				}
			}
		}
		if (bean.getItems() != null) {
			for (Item item : bean.getItems()) {
				try {
					masterDataDao.addItem(item);
				} catch (DuplicateKeyException e) {
					logger.error("Duplicate key in items : " + item.getItemName());
				}
			}
		}
		if (bean.getLocations() != null) {
			for (Location location : bean.getLocations()) {
				try {
					masterDataDao.addLocation(location);
				} catch (DuplicateKeyException e) {
					logger.error("Duplicate key in locations : " + location.getLocationName());
				}
			}
		}
		if (bean.getUserRoles() != null) {
			for (UserRole role : bean.getUserRoles()) {
				try {
					masterDataDao.addUserRole(role);
				} catch (DuplicateKeyException e) {
					logger.error("Duplicate key in roles : " + role.getUserRoleName());
				}
			}
		}
		if (bean.getVegetables() != null) {
			for (Vegetable vegetable : bean.getVegetables()) {
				try {
					masterDataDao.addvegetable(vegetable);
				} catch (DuplicateKeyException e) {
					logger.error("Duplicate key in vegetables : " + vegetable.getVegetableName());
				}
			}
		}
		logger.info("master data added successfully");
		bean = new MasterDataBean();
		bean.setStatusCode(1);
		bean.setStatusMessage("master data added successfully");
		return bean;

	}

}
