/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.Date;
import java.util.List;

import com.innoveyes.currymate.bean.PreparedItemBean;

/**
 * @author LakshmiKiran
 *
 */
public interface PreparedItemService {
	
	public PreparedItemBean savePreparedItem(List<PreparedItemBean> beans);
	
	public PreparedItemBean getItemsByStoreId(String storeId, Date createdDate);
	
}
