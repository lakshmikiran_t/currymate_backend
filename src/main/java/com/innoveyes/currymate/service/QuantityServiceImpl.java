/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.QuantityBean;
import com.innoveyes.currymate.dao.GroceryQuantityDao;
import com.innoveyes.currymate.dao.VegetableQuantityDao;
import com.innoveyes.currymate.model.GroceryQuantity;
import com.innoveyes.currymate.model.VegetableQuantity;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class QuantityServiceImpl implements QuantityService {

	private Logger logger = LogManager.getLogger(QuantityServiceImpl.class);

	@Autowired
	private GroceryQuantityDao groceryQuantityDao;

	@Autowired
	private VegetableQuantityDao vegetableQuantityDao;

	@Override
	public QuantityBean saveGroceryQuantity(QuantityBean bean) {
		logger.info("saving grocery quantity");
		try {
			GroceryQuantity quantity = groceryQuantityDao.saveGroceryQuantity(getGroceryQuality(bean));
			if (quantity == null) {
				logger.info("grocery quantity not saved");
				bean = new QuantityBean();
				bean.setStatusCode(0);
				bean.setStatusMessage("grocery quantity not saved");
				return bean;
			}
			bean = getGroceryQuantityBean(quantity);
			logger.info("grocery quantity saved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("grocery quantity saved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving grocery quantity", e);
			bean = new QuantityBean();
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving grocery quantity");
			return bean;
		}
	}

	@Override
	public QuantityBean saveVegetableQuantuty(QuantityBean bean) {
		logger.info("saving vegetable quantity");
		try {
			VegetableQuantity quantity = vegetableQuantityDao.saveVegetableQuantity(getVegetableQuantity(bean));
			if (quantity == null) {
				logger.info("vegetable quantity not saved");
				bean = new QuantityBean();
				bean.setStatusCode(0);
				bean.setStatusMessage("vegetable quantity not saved");
				return bean;
			}
			bean = getVegetableQualityBean(quantity);
			logger.info("vegetable quantity saved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("vegetable quantity saved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving vegetable quantity", e);
			bean = new QuantityBean();
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving vegetable quantity");
			return bean;
		}
	}

	@Override
	public QuantityBean getGroceryQuantityByStore(String groceryId, String storeId) {
		logger.info("retrieving grovery quantity by grocery id and store id");
		QuantityBean bean = new QuantityBean();
		try {
			GroceryQuantity quantity = groceryQuantityDao.getGroceryQuantityByStore(groceryId, storeId);
			if (quantity == null) {
				logger.info("no grocery quantity for grocery id : " + groceryId + " and store id : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage(
						"no grocery quantity for grocery id : " + groceryId + " and store id : " + storeId);
				return bean;
			}
			logger.info("grocery quantity retrieved successfully");
			bean = getGroceryQuantityBean(quantity);
			bean.setStatusCode(1);
			bean.setStatusMessage("grocery quantity retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving grocery quantity", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving grocery quantity");
			return bean;
		}
	}

	@Override
	public QuantityBean getVegetableQuantityByStore(String vegetableId, String storeId) {
		logger.info("retrieving vegetable quantity by grocery id and store id");
		QuantityBean bean = new QuantityBean();
		try {
			VegetableQuantity quantity = vegetableQuantityDao.getVegetableQuantityByStore(vegetableId, storeId);
			if (quantity == null) {
				logger.info("no grocery quantity for vegetable id : " + vegetableId + " and store id : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage(
						"no grocery quantity for grocery id : " + vegetableId + " and store id : " + storeId);
				return bean;
			}
			logger.info("vegetable quantity retrieved successfully");
			bean = getVegetableQualityBean(quantity);
			bean.setStatusCode(1);
			bean.setStatusMessage("vegetable quantity retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving vegetable quantity", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving vegetable quantity");
			return bean;
		}
	}

	@Override
	public QuantityBean getGroceriesForStore(String storeId) {
		logger.info("retrieving grocery quantities for store : " + storeId);
		QuantityBean bean = new QuantityBean();
		try {
			List<GroceryQuantity> quantities = groceryQuantityDao.getGroceriesForStore(storeId);
			if (quantities == null || quantities.isEmpty()) {
				logger.info("no grocery quantity for store id : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no grocery quantity for store id : " + storeId);
				return bean;
			}
			bean.setGroceries(quantities);
			logger.info("grocery quantities retrieved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("grocery quantities retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving grocery quantities for store : " + storeId, e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving grocery quantities for store : " + storeId);
			return bean;
		}
	}

	@Override
	public QuantityBean getVegetablesForStore(String storeId) {
		logger.info("retrieving vegetable quantities for store : " + storeId);
		QuantityBean bean = new QuantityBean();
		try {
			List<VegetableQuantity> quantities = vegetableQuantityDao.getVegetablesForStore(storeId);
			if (quantities == null || quantities.isEmpty()) {
				logger.info("no vegetable quantity for store id : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no vegetable quantity for store id : " + storeId);
				return bean;
			}
			bean.setVegetables(quantities);
			logger.info("vegetable quantities retrieved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("vegetable quantities retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving vegetable quantities for store : " + storeId, e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving vegetable quantities for store : " + storeId);
			return bean;
		}
	}

	@Override
	public QuantityBean getFinishedVegQuantityForStore(String storeId) {
		logger.info("retrieving finished vegetable quantities for store : " + storeId);
		QuantityBean bean = new QuantityBean();
		try {
			List<VegetableQuantity> quantities = vegetableQuantityDao.getFinishedVegQuantityForStore(storeId);
			if (quantities == null || quantities.isEmpty()) {
				logger.info("no finished vegetable quantity for store id : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no finished vegetable quantity for store id : " + storeId);
				return bean;
			}
			bean.setVegetables(quantities);
			logger.info("finished vegetable quantities retrieved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("finished vegetable quantities retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving finished vegetable quantities for store : " + storeId, e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving finished vegetable quantities for store : " + storeId);
			return bean;
		}
	}

	@Override
	public QuantityBean getFinishedGroceryByStore(String storeId) {
		logger.info("retrieving finished grocery quantities for store : " + storeId);
		QuantityBean bean = new QuantityBean();
		try {
			List<GroceryQuantity> quantities = groceryQuantityDao.getFinishedGroceryByStore(storeId);
			if (quantities == null || quantities.isEmpty()) {
				logger.info("no  finished grocery quantity for store id : " + storeId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no finished grocery quantity for store id : " + storeId);
				return bean;
			}
			bean.setGroceries(quantities);
			logger.info("finished grocery quantities retrieved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("finished grocery quantities retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving finished grocery quantities for store : " + storeId, e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while finished retrieving grocery quantities for store : " + storeId);
			return bean;
		}
	}

	private QuantityBean getGroceryQuantityBean(GroceryQuantity quantity) {
		QuantityBean bean = new QuantityBean();
		bean.setCostPerKG(quantity.getCostPerKG());
		bean.setCreatedDate(quantity.getCreatedDate());
		bean.setGroceryId(quantity.getGroceryId());
		bean.setGroceryName(quantity.getGroceryName());
		bean.setGroceryQuantityId(quantity.getGroceryQuantityId());
		bean.setIsFinished(quantity.getIsFinished());
		bean.setStoreId(quantity.getStoreId());
		bean.setRemainingQuantity(quantity.getRemainingQuantity());
		bean.setTotalGroceryCost(quantity.getTotalGroceryCost());
		bean.setTotalQuantity(quantity.getTotalQuantity());
		bean.setUpdatedDate(quantity.getUpdatedDate());
		bean.setUsedQuantity(quantity.getUsedQuantity());
		return bean;
	}

	private GroceryQuantity getGroceryQuality(QuantityBean quantity) {
		GroceryQuantity bean = new GroceryQuantity();
		bean.setCostPerKG(quantity.getCostPerKG());
		bean.setCreatedDate(new Date());
		bean.setGroceryId(quantity.getGroceryId());
		bean.setGroceryName(quantity.getGroceryName());
		bean.setGroceryQuantityId(quantity.getGroceryQuantityId());
		bean.setIsFinished(quantity.getIsFinished());
		bean.setStoreId(quantity.getStoreId());
		bean.setRemainingQuantity(quantity.getRemainingQuantity());
		bean.setTotalGroceryCost(quantity.getTotalGroceryCost());
		bean.setTotalQuantity(quantity.getTotalQuantity());
		bean.setUpdatedDate(new Date());
		bean.setUsedQuantity(quantity.getUsedQuantity());
		return bean;
	}

	private QuantityBean getVegetableQualityBean(VegetableQuantity quantity) {
		QuantityBean bean = new QuantityBean();
		bean.setCostPerKG(quantity.getCostPerKG());
		bean.setCreatedDate(quantity.getCreatedDate());
		bean.setVegetableId(quantity.getVegetableId());
		bean.setVegetableName(quantity.getVegetableName());
		bean.setVegetableQuantityId(quantity.getVegetableQuantityId());
		bean.setIsFinished(quantity.getIsFinished());
		bean.setStoreId(quantity.getStoreId());
		bean.setRemainingQuantity(quantity.getRemainingQuantity());
		bean.setTotalVegetableCost(quantity.getTotalVegetableCost());
		bean.setTotalQuantity(quantity.getTotalQuantity());
		bean.setUpdatedDate(quantity.getUpdatedDate());
		bean.setUsedQuantity(quantity.getUsedQuantity());
		return bean;
	}

	private VegetableQuantity getVegetableQuantity(QuantityBean quantity) {
		VegetableQuantity bean = new VegetableQuantity();
		bean.setCostPerKG(quantity.getCostPerKG());
		bean.setCreatedDate(new Date());
		bean.setVegetableId(quantity.getVegetableId());
		bean.setVegetableName(quantity.getVegetableName());
		bean.setVegetableQuantityId(quantity.getVegetableQuantityId());
		bean.setIsFinished(quantity.getIsFinished());
		bean.setStoreId(quantity.getStoreId());
		bean.setRemainingQuantity(quantity.getRemainingQuantity());
		bean.setTotalVegetableCost(quantity.getTotalVegetableCost());
		bean.setTotalQuantity(quantity.getTotalQuantity());
		bean.setUpdatedDate(new Date());
		bean.setUsedQuantity(quantity.getUsedQuantity());
		return bean;
	}
}
