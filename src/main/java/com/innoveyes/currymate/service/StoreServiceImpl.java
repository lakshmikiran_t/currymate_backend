/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.FavoriteBean;
import com.innoveyes.currymate.bean.StoreBean;
import com.innoveyes.currymate.dao.RatingAndFavoriteDao;
import com.innoveyes.currymate.dao.StoreDao;
import com.innoveyes.currymate.model.Favorite;
import com.innoveyes.currymate.model.Rating;
import com.innoveyes.currymate.model.Store;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class StoreServiceImpl implements StoreService {

	private Logger logger = LogManager.getLogger(StoreServiceImpl.class);

	@Autowired
	private StoreDao storeDao;

	@Autowired
	private RatingAndFavoriteDao ratingAndFavoriteDao;

	@Override
	public StoreBean saveStore(StoreBean bean) {
		try {
			Store store = getStore(bean);
			store = storeDao.saveStore(store);
			bean = getStoreBean(store);
			bean.setStatusCode(1);
			bean.setStatusMessage("Store created successfully");
			logger.info("Store created successfully");
			return bean;
		} catch (DuplicateKeyException e) {
			bean = new StoreBean();
			bean.setStatusCode(2);
			bean.setStatusMessage("Strore already exists with phone number : " + bean.getStorePhone());
			logger.error("Strore already exists with phone number : " + bean.getStorePhone());
			return bean;
		}
	}

	@Override
	public StoreBean findStoreByPhone(String storePhone) {
		logger.info("fectching store by store Id");
		StoreBean bean = new StoreBean();
		Store store = storeDao.findStoreByPhone(storePhone);
		if (store == null) {
			logger.warn("no store with store phone : " + storePhone);
			bean.setStatusCode(0);
			bean.setStatusMessage("no store with store phone : " + storePhone);
			return bean;
		}
		bean = getStoreBean(store);
		logger.info("store fetched successfully");
		bean.setStatusCode(1);
		bean.setStatusMessage("store fetched successfully");
		return bean;
	}

	@Override
	public StoreBean findStoresByLocation(String locationId) {
		logger.info("fectching store by location Id");
		StoreBean bean = new StoreBean();
		List<StoreBean> beans = new ArrayList<>();
		List<Store> stores = storeDao.findStoresByLocation(locationId);
		if (stores == null || stores.isEmpty()) {
			logger.warn("no stores with location Id : " + locationId);
			bean.setStatusCode(0);
			bean.setStatusMessage("no stores with location Id : " + locationId);
			return bean;
		}
		for (Store store : stores) {
			bean = getStoreBean(store);
			beans.add(bean);
			bean = new StoreBean();
		}
		logger.info("stores fetched successfully");
		bean.setStores(beans);
		bean.setStatusCode(1);
		bean.setStatusMessage("stores fetched successfully");
		return bean;
	}

	@Override
	public StoreBean findStoresByArea(String areaId) {
		logger.info("fectching store by area Id");
		StoreBean bean = new StoreBean();
		List<StoreBean> beans = new ArrayList<>();
		List<Store> stores = storeDao.findStoresByArea(areaId);
		if (stores == null || stores.isEmpty()) {
			logger.warn("no stores with area Id : " + areaId);
			bean.setStatusCode(0);
			bean.setStatusMessage("no stores with area Id : " + areaId);
			return bean;
		}
		for (Store store : stores) {
			bean = getStoreBean(store);
			beans.add(bean);
			bean = new StoreBean();
		}
		logger.info("stores fetched successfully");
		bean.setStores(beans);
		bean.setStatusCode(1);
		bean.setStatusMessage("stores fetched successfully");
		return bean;
	}

	@Override
	public StoreBean findStoresForUser(String userId) {
		logger.info("fectching store by user Id");
		StoreBean bean = new StoreBean();
		List<StoreBean> beans = new ArrayList<>();
		List<Store> stores = storeDao.findStoresByArea(userId);
		if (stores == null || stores.isEmpty()) {
			logger.warn("no stores with user Id : " + userId);
			bean.setStatusCode(0);
			bean.setStatusMessage("no stores with user Id : " + userId);
			return bean;
		}
		for (Store store : stores) {
			bean = getStoreBean(store);
			beans.add(bean);
			bean = new StoreBean();
		}
		logger.info("stores fetched successfully");
		bean.setStores(beans);
		bean.setStatusCode(1);
		bean.setStatusMessage("stores fetched successfully");
		return bean;
	}

	private Store getStore(StoreBean bean) {
		Store store = new Store();
		store.setAreaId(bean.getAreaId());
		store.setCreatedDate(new Date());
		store.setLocationId(bean.getLocationId());
		store.setPincode(bean.getPincode());
		store.setStoreAddress(bean.getStoreAddress());
		store.setStoreImages(bean.getStoreImages());
		store.setStoreName(bean.getStoreName());
		store.setStorePhone(bean.getStorePhone());
		store.setStroreId(bean.getStroreId());
		store.setUpdatedDate(new Date());
		store.setUserProfileId(bean.getUserProfileId());
		store.setRating(bean.getRating());
		return store;
	}

	private StoreBean getStoreBean(Store bean) {
		StoreBean store = new StoreBean();
		store.setAreaId(bean.getAreaId());
		store.setCreatedDate(bean.getCreatedDate());
		store.setLocationId(bean.getLocationId());
		store.setPincode(bean.getPincode());
		store.setStoreAddress(bean.getStoreAddress());
		store.setStoreImages(bean.getStoreImages());
		store.setStoreName(bean.getStoreName());
		store.setStorePhone(bean.getStorePhone());
		store.setStroreId(bean.getStroreId());
		store.setUpdatedDate(bean.getUpdatedDate());
		store.setUserProfileId(bean.getUserProfileId());
		store.setRating(findStoreRating(bean.getUserProfileId(), bean.getStroreId()));
		Favorite favorite = ratingAndFavoriteDao.isFavorite(bean.getUserProfileId(), bean.getStroreId());
		if (favorite != null) {
			store.setIsFavorite(true);
		}else {
			store.setIsFavorite(false);
		}
		return store;
	}

	@Override
	public void sendSMS(String mobile) {
		// TODO Auto-generated method stub
	}

	@Override
	public Integer findStoreRating(String userProfileId, String storeId) {
		List<Rating> ratings = ratingAndFavoriteDao.storeRating(userProfileId, storeId);
		Integer storeRating = 0;
		if (ratings != null && !ratings.isEmpty()) {
			for (Rating rating : ratings) {
				storeRating += rating.getRating();
			}
			storeRating = storeRating / ratings.size();
		}
		return storeRating;
	}

	@Override
	public FavoriteBean addFavorite(String userProfileId, String storeId) {
		FavoriteBean bean = new FavoriteBean();
		try {
			Store store = storeDao.findStoreById(storeId);
			if (store == null) {
				bean.setStatusCode(0);
				bean.setStatusMessage("No store with Id : " + storeId);
				logger.info("No store with Id : " + storeId);
				return bean;
			}
			Favorite favorite = ratingAndFavoriteDao.isFavorite(userProfileId, storeId);
			if (favorite != null) {
				bean.setStatusCode(0);
				bean.setStatusMessage("Store already added to favorite for user : " + userProfileId);
				logger.info("Store already added to favorite for user : " + userProfileId);
				return bean;
			}
			favorite = new Favorite();
			favorite.setUserProfileId(userProfileId);
			favorite.setStoreId(storeId);
			favorite.setCreatedDate(new Date());
			ratingAndFavoriteDao.addFavorite(favorite);
			bean.setStatusCode(1);
			bean.setStatusMessage("Store added as favorite to user : " + userProfileId);
			logger.info("Store added as favorite to user : " + userProfileId);
			return bean;
		} catch (Exception e) {
			logger.info("Exception while adding store to favorite", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("Exception while adding store to favorite");
			return bean;
		}
	}

	@Override
	public FavoriteBean deleteFavorite(String userProfileId, String storeId) {
		FavoriteBean bean = new FavoriteBean();
		try {
			Favorite favorite = ratingAndFavoriteDao.isFavorite(userProfileId, storeId);
			if (favorite == null) {
				bean.setStatusCode(0);
				bean.setStatusMessage("No favorite added for store : " + storeId);
				logger.info("No favorite added for store : " + storeId);
				return bean;
			}
			ratingAndFavoriteDao.deleteFavorite(userProfileId, storeId);
			bean.setStatusCode(1);
			bean.setStatusMessage("Favorite deleted successfully for user : " + userProfileId);
			logger.info("Favorite deleted successfully for user : " + userProfileId);
			return bean;
		} catch (Exception e) {
			logger.info("Exception while removing store from favorite", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("Exception while removing store from favorite");
			return bean;
		}
	}

	@Override
	public StoreBean getFavoriteStores(String userProfileId) {
		StoreBean bean = new StoreBean();
		List<StoreBean> beans = new ArrayList<>();
		try {
			List<Favorite> favorites = ratingAndFavoriteDao.getFavoriteStores(userProfileId);
			if (favorites == null || favorites.isEmpty()) {
				bean.setStatusCode(0);
				bean.setStatusMessage("No favorites added for the user : " + userProfileId);
				logger.info("No favorites added for the user : " + userProfileId);
				return bean;
			}
			for (Favorite favorite : favorites) {
				Store store = storeDao.findStoreById(favorite.getStoreId());
				bean = getStoreBean(store);
				beans.add(bean);
				bean = new StoreBean();
			}
			bean = new StoreBean();
			bean.setStatusCode(1);
			bean.setStatusMessage("Favorite stores retrieved successfully");
			bean.setStores(beans);
			logger.info("Favorite stores retrieved successfully");
			return bean;
		} catch (Exception e) {
			bean = new StoreBean();
			logger.info("Exception while retrieving favorite stores", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("Exception while retrieving favorite stores");
			return bean;
		}
	}

}
