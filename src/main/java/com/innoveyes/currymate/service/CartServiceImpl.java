/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.CartBean;
import com.innoveyes.currymate.bean.OrderBean;
import com.innoveyes.currymate.dao.CartDao;
import com.innoveyes.currymate.model.Cart;
import com.innoveyes.currymate.model.Order;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class CartServiceImpl implements CartService {

	private Logger logger = LogManager.getLogger(CartServiceImpl.class);

	@Autowired
	private CartDao cartDao;

	@Autowired
	private OrderService orderService;

	@Override
	public CartBean saveCart(CartBean bean) {
		logger.info("saving orders to cart");
		try {
			if (bean.getOrders() == null || bean.getOrders().isEmpty()) {
				logger.info("no orders to save");
				bean.setStatusCode(0);
				bean.setStatusMessage("no orders to save");
				return bean;
			}
			Cart cart = getCart(bean);
			cartDao.saveCart(cart);
			logger.info("orders saved to cart successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("orders saved to cart successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving orders to cart", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving orders to cart");
			return bean;
		}
	}

	@Override
	public CartBean findTodayCartsByUserAndStore(String userId, String storeId, Date cartedDate) {
		logger.info("retrieving today's orders from cart");
		CartBean bean = new CartBean();
		List<CartBean> beans = new ArrayList<>();
		try {
			List<Cart> carts = cartDao.findTodayCartsByUserAndStore(userId, storeId, cartedDate);
			if (carts == null || carts.isEmpty()) {
				logger.info("no orders in the cart for today");
				bean.setStatusCode(0);
				bean.setStatusMessage("no orders in the cart for today");
				return bean;
			}
			for (Cart cart : carts) {
				bean = getCartBean(cart);
				beans.add(bean);
				bean = new CartBean();
			}
			logger.info("orders in the cart are retrieved successfully for today");
			bean.setBeans(beans);
			bean.setStatusCode(1);
			bean.setStatusMessage("orders in the cart are retrieved successfully for today");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving today's orders from cart", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving today's orders from cart");
			return bean;
		}
	}

	@Override
	public CartBean findAllCartsByUserAndStore(String userId, String storeId) {
		logger.info("retrieving all orders from cart");
		CartBean bean = new CartBean();
		List<CartBean> beans = new ArrayList<>();
		try {
			List<Cart> carts = cartDao.findAllCartsByUserAndStore(userId, storeId);
			if (carts == null || carts.isEmpty()) {
				logger.info("no orders in any cart");
				bean.setStatusCode(0);
				bean.setStatusMessage("no orders in any cart");
				return bean;
			}
			for (Cart cart : carts) {
				bean = getCartBean(cart);
				beans.add(bean);
				bean = new CartBean();
			}
			logger.info("all orders in the cart are retrieved successfully");
			bean.setBeans(beans);
			bean.setStatusCode(1);
			bean.setStatusMessage("orders in the cart are retrieved successfully for today");
			return bean;
		} catch (Exception e) {
			logger.error("exception while retrieving all orders from cart", e);
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while retrieving all orders from cart");
			return bean;
		}
	}

	private Cart getCart(CartBean cartBean) {
		Cart cart = new Cart();
		List<Order> orders = new ArrayList<>();
		cart.setCartedDate(new Date());
		cart.setCartId(cartBean.getCartId());
		cart.setCheckOutCost(cartBean.getCheckOutCost());
		cart.setIsPaid(cartBean.getIsPaid());
		for (OrderBean bean : cartBean.getOrders()) {
			Order order = orderService.getOrder(bean);
			orders.add(order);
		}
		cart.setOrders(orders);
		return cart;
	}

	private CartBean getCartBean(Cart cartBean) {
		CartBean cart = new CartBean();
		List<OrderBean> orders = new ArrayList<>();
		cart.setCartedDate(new Date());
		cart.setCartId(cartBean.getCartId());
		cart.setCheckOutCost(cartBean.getCheckOutCost());
		cart.setIsPaid(cartBean.getIsPaid());
		for (Order bean : cartBean.getOrders()) {
			OrderBean order = orderService.getOrderBean(bean);
			orders.add(order);
		}
		cart.setOrders(orders);
		return cart;
	}

}
