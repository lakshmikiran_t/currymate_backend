/**
 * 
 */
package com.innoveyes.currymate.service;

import java.io.IOException;

import javax.mail.MessagingException;

import com.innoveyes.currymate.bean.Mail;

/**
 * @author LakshmiKiran
 *
 */
public interface EmailService {

	public void sendSimpleMessage(Mail mail) throws MessagingException, IOException;
}
