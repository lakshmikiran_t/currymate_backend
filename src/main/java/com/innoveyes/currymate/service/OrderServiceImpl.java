/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innoveyes.currymate.bean.OrderBean;
import com.innoveyes.currymate.dao.OrderDao;
import com.innoveyes.currymate.dao.PreparedItemDao;
import com.innoveyes.currymate.model.Order;
import com.innoveyes.currymate.model.PreparedItem;

/**
 * @author LakshmiKiran
 *
 */
@Service
public class OrderServiceImpl implements OrderService {

	private Logger logger = LogManager.getLogger(OrderServiceImpl.class);

	@Autowired
	private OrderDao orderDao;

	@Autowired
	private PreparedItemDao preparedItemDao;

	@Override
	public OrderBean saveOrder(OrderBean bean) {
		logger.info("creating an order for user : " + bean.getUserProfileId());
		try {
			PreparedItem item = preparedItemDao.findPreparedItemById(bean.getPreparedItemId());
			if (item == null) {
				logger.info("no prepared item with id : " + bean.getPreparedItemId());
				bean.setStatusCode(0);
				bean.setStatusMessage("no prepared item with id : " + bean.getPreparedItemId());
				return bean;
			}
			if (item.getIsFinished()) {
				logger.info("cannot order, prepared item is finished");
				bean.setStatusCode(0);
				bean.setStatusMessage("cannot order, prepared item is finished");
				return bean;
			}
			if (item.getTotalPacks() == item.getOrderedPacks() + bean.getNoOfPacks()) {
				item.setOrderedPacks(item.getOrderedPacks() + bean.getNoOfPacks());
				item.setIsFinished(true);
				preparedItemDao.savePreparedItem(item);
				Order order = getOrder(bean);
				orderDao.saveOrder(order);
				logger.info("order saved successfully");
				bean.setStatusCode(1);
				bean.setStatusMessage("order saved successfully");
				return bean;
			}
			item.setOrderedPacks(item.getOrderedPacks() + bean.getNoOfPacks());
			preparedItemDao.savePreparedItem(item);
			Order order = getOrder(bean);
			orderDao.saveOrder(order);
			logger.info("order saved successfully");
			bean.setStatusCode(1);
			bean.setStatusMessage("order saved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving an order");
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving an order");
			return bean;
		}
	}

	@Override
	public OrderBean findOrderByUserStoreAndDate(String userId, String storeId, Date orderedDate) {
		logger.info("retrieving the order for user store and ordered date");
		OrderBean bean = new OrderBean();
		List<OrderBean> beans = new ArrayList<>();
		try {
			List<Order> orders = orderDao.findOrderByUserStoreAndDate(userId, storeId, orderedDate);
			if (orders == null || orders.isEmpty()) {
				logger.info("no orders for the user : " + userId + " on date : " + orderedDate);
				bean.setStatusCode(0);
				bean.setStatusMessage("no orders for the user : " + userId + " on date : " + orderedDate);
				return bean;
			}
			for (Order order : orders) { 
				bean = getOrderBean(order);
				beans.add(bean);
				bean = new OrderBean();
			}
			logger.info("orders retrieved successfully");
			bean.setBeans(beans);
			bean.setStatusCode(1);
			bean.setStatusMessage("orders retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving an order");
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving an order");
			return bean;
		}
	}

	@Override
	public Order getOrder(OrderBean bean) {
		Order order = new Order();
		order.setIsCancelled(false);
		order.setIsPaymentDone(false);
		order.setItemCost(bean.getItemCost());
		order.setNoOfPacks(bean.getNoOfPacks());
		order.setOrderCost(bean.getOrderCost());
		order.setOrderedDate(new Date());
		order.setOrderId(bean.getOrderId());
		order.setPreparedItemId(bean.getPreparedItemId());
		order.setStoreId(bean.getStoreId());
		order.setUserProfileId(bean.getUserProfileId());
		return order;
	}

	@Override
	public OrderBean getOrderBean(Order bean) {
		OrderBean order = new OrderBean();
		order.setIsCancelled(bean.getIsCancelled());
		order.setIsPaymentDone(bean.getIsPaymentDone());
		order.setItemCost(bean.getItemCost());
		order.setNoOfPacks(bean.getNoOfPacks());
		order.setOrderCost(bean.getOrderCost());
		order.setOrderedDate(bean.getOrderedDate());
		order.setOrderId(bean.getOrderId());
		order.setPreparedItemId(bean.getPreparedItemId());
		order.setStoreId(bean.getStoreId());
		order.setUserProfileId(bean.getUserProfileId());
		return order;
	}

	@Override
	public OrderBean findOrderByUserStore(String userId, String storeId) {
		logger.info("retrieving the order for user store and ordered date");
		OrderBean bean = new OrderBean();
		List<OrderBean> beans = new ArrayList<>();
		try {
			List<Order> orders = orderDao.findOrderByUserStore(userId, storeId);
			if (orders == null || orders.isEmpty()) {
				logger.info("no orders for the user : " + userId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no orders for the user : " + userId);
				return bean;
			}
			for (Order order : orders) { 
				bean = getOrderBean(order);
				beans.add(bean);
				bean = new OrderBean();
			}
			logger.info("orders retrieved successfully");
			bean.setBeans(beans);
			bean.setStatusCode(1);
			bean.setStatusMessage("orders retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving an order");
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving an order");
			return bean;
		}
	}

	@Override
	public OrderBean findOrderByUserStoreAndCancelled(String userId, String storeId) {
		logger.info("retrieving the order for user store and ordered date");
		OrderBean bean = new OrderBean();
		List<OrderBean> beans = new ArrayList<>();
		try {
			List<Order> orders = orderDao.findOrderByUserStoreAndCancelled(userId, storeId);
			if (orders == null || orders.isEmpty()) {
				logger.info("no orders for the user : " + userId);
				bean.setStatusCode(0);
				bean.setStatusMessage("no orders for the user : " + userId);
				return bean;
			}
			for (Order order : orders) { 
				bean = getOrderBean(order);
				beans.add(bean);
				bean = new OrderBean();
			}
			logger.info("orders retrieved successfully");
			bean.setBeans(beans);
			bean.setStatusCode(1);
			bean.setStatusMessage("orders retrieved successfully");
			return bean;
		} catch (Exception e) {
			logger.error("exception while saving an order");
			bean.setStatusCode(2);
			bean.setStatusMessage("exception while saving an order");
			return bean;
		}
	}

}
