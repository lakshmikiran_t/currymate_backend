/**
 * 
 */
package com.innoveyes.currymate.service;

import com.innoveyes.currymate.bean.RatingBean;
import com.innoveyes.currymate.bean.UserProfileBean;

/**
 * @author LakshmiKiran
 *
 */
public interface UserProfileService {
	
	public String isUserLoggedIn(String userId);
	
	public UserProfileBean signUp(UserProfileBean bean) throws Exception;
	
	public UserProfileBean login(UserProfileBean bean) throws Exception;
	
	public String sendSMS(String mobile);
	
	public UserProfileBean verifyUser(String mobile, String otp);
	
	public UserProfileBean findUserLoginStatus(String sessionToken);
	
	public RatingBean rateStore(RatingBean bean);
}
