/**
 * 
 */
package com.innoveyes.currymate.service;

import java.util.Date;

import com.innoveyes.currymate.bean.CartBean;

/**
 * @author LakshmiKiran
 *
 */
public interface CartService {
	
	public CartBean saveCart(CartBean bean);

	public CartBean findTodayCartsByUserAndStore(String userId, String storeId, Date cartedDate);
	
	public CartBean findAllCartsByUserAndStore(String userId, String storeId);
}
