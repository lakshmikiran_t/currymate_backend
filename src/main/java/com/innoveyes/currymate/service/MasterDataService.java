/**
 * 
 */
package com.innoveyes.currymate.service;

import com.innoveyes.currymate.bean.MasterDataBean;

/**
 * @author LakshmiKiran
 *
 */
public interface MasterDataService {
	
	public MasterDataBean masterData();
	
	public MasterDataBean saveMasterData(MasterDataBean bean);
}
