/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.innoveyes.currymate.bean.UserProfileBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.UserProfileService;

/**
 * @author LakshmiKiran
 *
 */
public class BaseController {
	
	@Autowired
	private UserProfileService userProfileService;
	
	private Logger logger = LogManager.getLogger(BaseController.class);
	
	public Map<Object, Object> isUserLoggedIn(String sessionToken){
		Map<Object, Object> res = new LinkedHashMap<>();
		UserProfileBean bean = userProfileService.findUserLoginStatus(sessionToken);
		if(bean.getStatusCode() == 0) {
			logger.info(bean.getStatusMessage());
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.DATA.value(), new String());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.LOCKED);
			return res;
		}
		logger.info("actice session");
		res.put("userId", bean.getUserId());
		res.put("sessionToken", bean.getSessionToken());
		res.put("userRoleId", bean.getRoleId());
		res.put(Message.STATUS_CODE.value(), HttpStatus.OK);
		return res;
	}
	
	
}
