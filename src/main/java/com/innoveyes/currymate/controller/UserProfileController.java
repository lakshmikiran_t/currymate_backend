/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innoveyes.currymate.bean.Mail;
import com.innoveyes.currymate.bean.RatingBean;
import com.innoveyes.currymate.bean.UserProfileBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.EmailService;
import com.innoveyes.currymate.service.UserProfileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LakshmiKiran
 *
 */
@RestController
@RequestMapping("/user")
@Api(value = "User Profile Controller")
public class UserProfileController extends BaseController {

	private Logger logger = LogManager.getLogger(UserProfileController.class);

	@Autowired
	private UserProfileService userProfileService;

	@ApiOperation(value = "User signup, All fields are mandatory")
	@RequestMapping(value = "/signUp", method = RequestMethod.POST, produces = { "application/json" })
	public Map<Object, Object> signUp(@RequestBody UserProfileBean bean) throws Exception {
		Map<Object, Object> res = new LinkedHashMap<>();
		if (bean.getFirstName() == null || bean.getAreaId() == null || bean.getEmail() == null
				|| bean.getLastName() == null || bean.getLocationId() == null || bean.getMobile() == null
				|| bean.getPassword() == null) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), "All fields are mandatory");
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_ACCEPTABLE);
			logger.warn("All fields are mandatory");
			return res;
		}
		if (!bean.getRoleId().equals(Message.ROLE_USER.value())) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), "Admin or Super admin cannot be signed up");
			res.put(Message.STATUS_CODE.value(), HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
			logger.warn("Admin or Super admin cannot be signed up");
			return res;
		}
		bean = userProfileService.signUp(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Exception in user profile contoller");
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn("Not valid attributes");
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info("User profile created successfully : " + bean);
		return res;
	}

	@ApiOperation(value = "User login, Mobile and Password fields are mandatory")
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = { "application/json" })
	public Map<Object, Object> login(@RequestBody UserProfileBean bean) throws Exception {

		Map<Object, Object> res = new LinkedHashMap<>();
		if (bean.getMobile() == null || bean.getPassword() == null) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), "All fields are mandatory");
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_ACCEPTABLE);
			logger.warn("All fields are mandatory");
			return res;
		}
		bean = userProfileService.login(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Exception in user profile contoller");
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn("Not valid attributes");
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info("User logged in successfully : " + bean);
		return res;
	}

	@ApiOperation(value = "Verify User, Mobile and otp are mandatory")
	@RequestMapping(value = "/verify", method = RequestMethod.GET, produces = { "application/json" })
	public Map<Object, Object> verifyUser(@RequestParam("mobile") String mobile, @RequestParam("otp") String otp)
			throws JSONException {

		Map<Object, Object> res = new LinkedHashMap<>();
		UserProfileBean bean = userProfileService.verifyUser(mobile, otp);
		if (mobile == null || otp == null) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), "All fields are mandatory");
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_ACCEPTABLE);
			logger.warn("All fields are mandatory");
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn("Not valid attributes");
			return res;
		}
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Exception in user profile contoller");
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info("User verified successfully : " + bean);
		return res;
	}

	@Autowired
	private EmailService emailService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "email", method = RequestMethod.GET, produces = "application/json")
	public Map<Object, Object> sendEmail() throws MessagingException, IOException {
		Map<Object, Object> map = new LinkedHashMap<>();
		Mail mail = new Mail();
		mail.setFrom("ceo@innoveyes.com");
		mail.setTo("saithuthuku18@gmail.com");
		mail.setSubject("Currymate sample mail");

		Map model = new HashMap();
		model.put("name", "Sai");
		model.put("location", "Hyderbad");
		model.put("signature", "Currymate");
		mail.setModel(model);
		emailService.sendSimpleMessage(mail);
		map.put("status", "success");
		return map;
	}

	@ApiOperation(value = "Rating a store")
	@RequestMapping(value = "/rateStore", method = RequestMethod.GET, produces = { "application/json" })
	public Map<Object, Object> rateStore(@RequestParam("sessionToken") String sessionToken,
			@RequestParam("userProfileId") String userProfileId, @RequestParam("storeId") String storeId) {

		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_USER.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		RatingBean bean = new RatingBean();
		bean.setUserProfileId(userProfileId);
		bean.setStoreId(storeId);
		bean = userProfileService.rateStore(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Exception in user profile contoller");
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage() + bean);
		return res;
	}
}
