/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innoveyes.currymate.bean.QuantityBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.QuantityService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LakshmiKiran
 *
 */
@RestController
@RequestMapping("/quantity")
@Api(value = "all the services for quantity management lies here")
public class QuantityController extends BaseController {

	private Logger logger = LogManager.getLogger(QuantityController.class);

	@Autowired
	private QuantityService quantityService;

	@RequestMapping(value = "/grocery/save", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "saving grocery quality")
	public Map<Object, Object> saveGroceryQuantity(@RequestBody QuantityBean bean,
			@RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.saveGroceryQuantity(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/vegetable/save", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "saving grocery quality")
	public Map<Object, Object> saveVegetableQuantity(@RequestBody QuantityBean bean,
			@RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.saveVegetableQuantuty(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/grocery/id/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "retrieve grocery quantity by grocery and store")
	public Map<Object, Object> getGroceryByIdAndStore(@RequestParam("groceryId") String groceryId,
			@RequestParam("storeId") String storeId, @RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		QuantityBean bean = new QuantityBean();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.getGroceryQuantityByStore(groceryId, storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/vegetable/id/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "retrieve vegetable quantity by vegetableId and store")
	public Map<Object, Object> getVegetableByIdAndStore(@RequestParam("vegetableId") String vegetableId,
			@RequestParam("storeId") String storeId, @RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		QuantityBean bean = new QuantityBean();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.getVegetableQuantityByStore(vegetableId, storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/vegetable/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "retrieve vegetable quantity by and store")
	public Map<Object, Object> getVegetableQuantitiesByStore(@RequestParam("storeId") String storeId,
			@RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		QuantityBean bean = new QuantityBean();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.getVegetablesForStore(storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getVegetables());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/grocery/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "retrieve grocery quantity by store")
	public Map<Object, Object> getGroceryQuantitiesByStore(@RequestParam("storeId") String storeId,
			@RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		QuantityBean bean = new QuantityBean();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.getGroceriesForStore(storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getGroceries());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/grocery/finished/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "retrieve finished grocery quantity by store")
	public Map<Object, Object> getFinishedQuantitiesByStore(@RequestParam("storeId") String storeId,
			@RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		QuantityBean bean = new QuantityBean();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.getFinishedGroceryByStore(storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getGroceries());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/vegetable/finished/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "retrieve finished vegetable quantity by store")
	public Map<Object, Object> getFinishedVegetableQuantitiesByStore(@RequestParam("storeId") String storeId,
			@RequestParam("sessionToken") String sessionToken) {
		Map<Object, Object> res = new LinkedHashMap<>();
		QuantityBean bean = new QuantityBean();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.DATA.value(), new String());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = quantityService.getFinishedVegQuantityForStore(storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getVegetables());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}
}
