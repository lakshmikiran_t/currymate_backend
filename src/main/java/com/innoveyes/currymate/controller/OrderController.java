/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innoveyes.currymate.bean.OrderBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LakshmiKiran
 *
 */
@RestController
@RequestMapping("/order")
@Api(value = "all the services for orders lies here")
public class OrderController extends BaseController {

	private Logger logger = LogManager.getLogger(OrderController.class);

	@Autowired
	private OrderService orderService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "service to save order")
	public Map<Object, Object> saveOrder(@RequestBody OrderBean bean,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking user login status");
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_USER.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());

			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = orderService.saveOrder(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/user/store/date", method = RequestMethod.GET, produces = { "application/json" })
	public Map<Object, Object> findOrderByUserStoreAndDate(@RequestParam("userId") String userId,
			@RequestParam("storeId") String storeId, @RequestParam("orderedDate") Date orderedDate,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking user login status");
		OrderBean bean = new OrderBean();
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_USER.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());

			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = orderService.findOrderByUserStoreAndDate(userId, storeId, orderedDate);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getBeans());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/user/store", method = RequestMethod.GET, produces = { "application/json" })
	public Map<Object, Object> findOrderByUserStore(@RequestParam("userId") String userId,
			@RequestParam("storeId") String storeId, @RequestParam("sessionToken") String sessionToken) {
		logger.info("checking user login status");
		OrderBean bean = new OrderBean();
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_USER.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());

			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = orderService.findOrderByUserStore(userId, storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getBeans());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}
	
	@RequestMapping(value = "/user/store/cancelled", method = RequestMethod.GET, produces = { "application/json" })
	public Map<Object, Object> findOrderByUserStoreAndCancelled(@RequestParam("userId") String userId,
			@RequestParam("storeId") String storeId, @RequestParam("sessionToken") String sessionToken) {
		logger.info("checking user login status");
		OrderBean bean = new OrderBean();
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_USER.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());

			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean = orderService.findOrderByUserStoreAndCancelled(userId, storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getBeans());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}
}
