/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innoveyes.currymate.bean.PreparedItemBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.PreparedItemService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LakshmiKiran
 *
 */
@RestController
@RequestMapping("/item")
@Api(value = "all the item services lies here")
public class ItemController extends BaseController {

	private Logger logger = LogManager.getLogger(ItemController.class);

	@Autowired
	private PreparedItemService preparedItemService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "service for saving prepared item")
	public Map<Object, Object> savePreparedItem(@RequestBody List<PreparedItemBean> beans,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session and admin permissions");
		Map<Object, Object> res = isUserLoggedIn(sessionToken);
		if (res.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return res;
		}
		if (!res.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			res.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return res;
		}
		if (beans == null || beans.isEmpty()) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), "input request cannot be null");
			res.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("input request cannot be null");
			return res;
		}
		PreparedItemBean bean = preparedItemService.savePreparedItem(beans);
		if (bean.getStatusCode() == 2) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res = new LinkedHashMap<>();
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/store", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "service for retrieving prepared items")
	public Map<Object, Object> getItemsForStrore(@RequestParam("storeId") String storeId,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session and admin permissions");
		Map<Object, Object> res = isUserLoggedIn(sessionToken);
		if (res.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return res;
		}
		PreparedItemBean bean = preparedItemService.getItemsByStoreId(storeId, null);
		if (bean.getStatusCode() == 2) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res = new LinkedHashMap<>();
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getBeans());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/store/date", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "service for retrieving prepared items")
	public Map<Object, Object> getItemsForStroreAndDate(@RequestParam("storeId") String storeId,
			@RequestParam("createdDate") Date createdDate, @RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session and admin permissions");
		Map<Object, Object> res = isUserLoggedIn(sessionToken);
		if (res.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return res;
		}
		PreparedItemBean bean = preparedItemService.getItemsByStoreId(storeId, createdDate);
		if (bean.getStatusCode() == 2) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res = new LinkedHashMap<>();
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res = new LinkedHashMap<>();
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getBeans());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}
}
