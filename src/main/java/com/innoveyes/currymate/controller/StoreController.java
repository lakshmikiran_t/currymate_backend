/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innoveyes.currymate.bean.FavoriteBean;
import com.innoveyes.currymate.bean.StoreBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.StoreService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author LakshmiKiran
 *
 */
@RestController
@RequestMapping("/store")
@Api(value = "All the store operation services lies here")
public class StoreController extends BaseController {

	private Logger logger = LogManager.getLogger(StoreController.class);

	@Autowired
	private StoreService storeService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "Save a store")
	public Map<Object, Object> saveStore(@RequestBody StoreBean bean,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());

			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		if (bean.getAreaId() == null || bean.getLocationId() == null || bean.getPincode() == null
				|| bean.getStoreAddress() == null || bean.getStoreName() == null || bean.getStorePhone() == null
				|| bean.getUserProfileId() == null) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), "All fields are mandatory");
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_ACCEPTABLE);
			logger.error("All fields are mandatory");
			return res;
		}
		bean = storeService.saveStore(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/storePhone", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "find a store by store phone")
	public Map<Object, Object> findStoreById(@RequestParam("storePhone") String storePhone,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		StoreBean bean = storeService.findStoreByPhone(storePhone);
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_FOUND);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/location", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "find a store by location Id")
	public Map<Object, Object> findStoreByLocation(@RequestParam("locationId") String locationId,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		StoreBean bean = storeService.findStoresByLocation(locationId);
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());

			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_FOUND);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getStores());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/area", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "find a store by area Id")
	public Map<Object, Object> findStoreByArea(@RequestParam("areaId") String areaId,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		StoreBean bean = storeService.findStoresByArea(areaId);
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_FOUND);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getStores());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/id", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "find a store by user Id")
	public Map<Object, Object> findStoreByUser(@RequestParam("userProfileId") String userProfileId,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		StoreBean bean = storeService.findStoresForUser(userProfileId);
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_FOUND);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getStores());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/favorite", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "Save a store")
	public Map<Object, Object> addFavorite(@RequestParam("userProfileId") String userProfileId,
			@RequestParam("storeId") String storeId, @RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_USER.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		if (userProfileId == null || storeId == null) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), "All fields are mandatory");
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_ACCEPTABLE);
			logger.error("All fields are mandatory");
			return res;
		}
		FavoriteBean bean = storeService.addFavorite(userProfileId, storeId);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_ACCEPTABLE);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/favorite", method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "delete a favorite store")
	public Map<Object, Object> deleteFavorite(@RequestParam("userProfileId") String userProfileId,
			@RequestParam("storeId") String storeId, @RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		FavoriteBean bean = storeService.deleteFavorite(userProfileId, storeId);
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_FOUND);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info(bean.getStatusMessage());
		return res;
	}

	@RequestMapping(value = "/favorite/stores/user", method = RequestMethod.GET, produces = "application/json")
	@ApiOperation(value = "find a favorite stores for user")
	public Map<Object, Object> deleteFavorite(@RequestParam("userProfileId") String userProfileId,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking the session");
		Map<Object, Object> res = new LinkedHashMap<Object, Object>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		StoreBean bean = storeService.getFavoriteStores(userProfileId);
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.NOT_FOUND);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean.getStores());
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info(bean.getStatusMessage());
		return res;
	}
}
