/**
 * 
 */
package com.innoveyes.currymate.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.innoveyes.currymate.bean.MasterDataBean;
import com.innoveyes.currymate.bean.UserProfileBean;
import com.innoveyes.currymate.message.Message;
import com.innoveyes.currymate.service.MasterDataService;
import com.innoveyes.currymate.service.UserProfileService;

import io.swagger.annotations.Api;

/**
 * @author LakshmiKiran
 *
 */
@RestController
@RequestMapping("/admin")
@Api(value = "Master data and admin services")
public class MasterController extends BaseController {

	private Logger logger = LogManager.getLogger(MasterController.class);

	@Autowired
	private MasterDataService masterDataService;

	@Autowired
	private UserProfileService userProfileService;

	@RequestMapping(value = "/masterData", method = RequestMethod.POST, produces = { "application/json" })
	public Map<Object, Object> addMasterData(@RequestBody MasterDataBean bean,
			@RequestParam("sessionToken") String sessionToken) {
		logger.info("checking user login status");
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_SUPER_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}

		bean = masterDataService.saveMasterData(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Exception in master contoller");
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info("master data saved successfully");
		return res;
	}

	@RequestMapping(value = "/masterData", method = RequestMethod.GET, produces = { "application/json" })
	public Map<Object, Object> masterData(@RequestParam("sessionToken") String sessionToken) {

		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		Map<Object, Object> res = new LinkedHashMap<>();
		MasterDataBean bean = masterDataService.masterData();
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Exception in Master contoller");
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.ACCEPTED);
		logger.info("master data retrieved successfully");
		return res;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
	public Map<Object, Object> createAdmin(@RequestBody UserProfileBean bean,
			@RequestParam("sessionToken") String sessionToken) throws Exception {

		logger.info("checking the session and admin permissions");
		Map<Object, Object> res = new LinkedHashMap<>();
		Map<Object, Object> statusRes = isUserLoggedIn(sessionToken);
		if (statusRes.get(Message.STATUS_CODE.value()).equals(HttpStatus.LOCKED)) {
			logger.warn("session expired, please login to contontinue");
			return statusRes;
		}
		if (!statusRes.get("userRoleId").equals(Message.ROLE_SUPER_ADMIN.value())) {
			statusRes = new LinkedHashMap<Object, Object>();
			statusRes.put(Message.STATUS.value(), Message.FAILURE.value());
			statusRes.put(Message.MESSAGE.value(), "you don't have permissions to access this service");
			statusRes.put(Message.STATUS_CODE.value(), HttpStatus.UNAUTHORIZED);
			logger.warn("you don't have permissions to access this service");
			return statusRes;
		}
		bean.setRoleId(Message.ROLE_ADMIN.value());
		bean = userProfileService.signUp(bean);
		if (bean.getStatusCode() == 2) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error(bean.getStatusMessage());
			return res;
		}
		if (bean.getStatusCode() == 0) {
			res.put(Message.STATUS.value(), Message.FAILURE.value());
			res.put(Message.MESSAGE.value(), bean.getStatusMessage());
			res.put(Message.STATUS_CODE.value(), HttpStatus.CONFLICT);
			logger.warn(bean.getStatusMessage());
			return res;
		}
		res.put(Message.STATUS.value(), Message.SUCCESS.value());
		res.put(Message.DATA.value(), bean);
		res.put(Message.MESSAGE.value(), bean.getStatusMessage());
		res.put(Message.STATUS_CODE.value(), HttpStatus.CREATED);
		logger.info(bean.getStatusMessage() + bean);
		return res;

	}
}
