/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.Date;
import java.util.List;

import com.innoveyes.currymate.model.Order;

/**
 * @author LakshmiKiran
 *
 */
public interface OrderDao {

	public void saveOrder(Order order);

	public List<Order> findOrderByUserStoreAndDate(String userId, String storeId, Date orderedDate);
	
	public List<Order> findOrderByUserStore(String userId, String storeId);
	
	public List<Order> findOrderByUserStoreAndCancelled(String userId, String storeId);

}
