/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.Date;
import java.util.List;

import com.innoveyes.currymate.model.PreparedItem;

/**
 * @author LakshmiKiran
 *
 */
public interface PreparedItemDao {
	
	public void savePreparedItem(PreparedItem bean);
	
	public PreparedItem findPreparedItemById(String preparedItemId);
	
	public List<PreparedItem> getItemsByStore(String storeId);
	
	public List<PreparedItem> getItemsByStoreAndDate(String storeId, Date createdDate);
}
