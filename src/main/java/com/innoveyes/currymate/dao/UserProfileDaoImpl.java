/**
 * 
 */
package com.innoveyes.currymate.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.UserLoginStatus;
import com.innoveyes.currymate.model.UserProfile;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class UserProfileDaoImpl implements UserProfileDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public UserProfile findUserById(String userId) {
		Query query = Query.query(Criteria.where("userId").is(userId));
		return mongoTemplate.findOne(query, UserProfile.class);
	}

	@Override
	public UserProfile findUserByMobile(String mobile) {
		Query query = Query.query(Criteria.where("mobile").is(mobile));
		return mongoTemplate.findOne(query, UserProfile.class);
	}

	@Override
	public UserLoginStatus isUserLoggedIn(String userId) {
		Query query = Query
				.query(Criteria.where("userId").is(userId).andOperator(Criteria.where("isLoggedOut").is(false)));
		return mongoTemplate.findOne(query, UserLoginStatus.class);
	}

	@Override
	public UserProfile signUp(UserProfile bean) {
		mongoTemplate.save(bean);
		return findUserByMobile(bean.getMobile());
	}

	@Override
	public void updateUser(UserProfile bean) {
		mongoTemplate.save(bean);
	}

	@Override
	public void saveUserLoginStatus(UserLoginStatus status) {
		mongoTemplate.save(status);
	}

	@Override
	public UserLoginStatus findUserLoginStatus(String sessionToken) {
		Query query = Query.query(
				Criteria.where("sessionToken").is(sessionToken).andOperator(Criteria.where("isLoggedOut").is(false)));
		return mongoTemplate.findOne(query, UserLoginStatus.class);
	}

	@Override
	public UserLoginStatus findLoginStatusById(String userId) {
		Query query = Query.query(
				Criteria.where("userId").is(userId).andOperator(Criteria.where("isLoggedOut").is(false)));
		return mongoTemplate.findOne(query, UserLoginStatus.class);
	}

}
