/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import com.innoveyes.currymate.model.Favorite;
import com.innoveyes.currymate.model.Rating;

/**
 * @author Lakshmi Kiran
 *
 */
public interface RatingAndFavoriteDao {
	
	public void rateStore(Rating rating);
	
	public List<Rating> storeRating(String userProfileId, String storeId);
	
	public void addFavorite(Favorite favorite);
	
	public Favorite isFavorite(String userProfileId, String storeId);
	
	public void deleteFavorite(String userProfileId, String storeId);
	
	public List<Favorite> getFavoriteStores(String userProfileId);
	
}
