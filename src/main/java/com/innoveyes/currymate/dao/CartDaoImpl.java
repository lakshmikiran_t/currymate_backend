/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.Cart;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class CartDaoImpl implements CartDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void saveCart(Cart cart) {
		mongoTemplate.save(cart);
	}

	@Override
	public List<Cart> findTodayCartsByUserAndStore(String userId, String storeId, Date cartedDate) {
		Query query = Query.query(Criteria.where("userId").is(userId).andOperator(Criteria.where("storeId").is(storeId))
				.andOperator(Criteria.where("cartedDate").is(cartedDate)));
		return mongoTemplate.find(query, Cart.class);
	}

	@Override
	public List<Cart> findAllCartsByUserAndStore(String userId, String storeId) {
		Query query = Query
				.query(Criteria.where("userId").is(userId).andOperator(Criteria.where("storeId").is(storeId)));
		return mongoTemplate.find(query, Cart.class);
	}

}
