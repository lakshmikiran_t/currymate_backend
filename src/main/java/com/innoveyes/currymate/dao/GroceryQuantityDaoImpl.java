/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.GroceryQuantity;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class GroceryQuantityDaoImpl implements GroceryQuantityDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public GroceryQuantity getGroceryQuantityByStore(String groceryId, String storeId) {
		Query query = Query
				.query(Criteria.where("groceryId").is(groceryId).andOperator(Criteria.where("storeId").is(storeId))
						.andOperator(Criteria.where("isFinished").is(false)));
		return mongoTemplate.findOne(query, GroceryQuantity.class);
	}

	@Override
	public GroceryQuantity saveGroceryQuantity(GroceryQuantity quantity) {
		mongoTemplate.save(quantity);
		return getGroceryQuantityByStore(quantity.getGroceryId(), quantity.getStoreId());
	}

	@Override
	public List<GroceryQuantity> getGroceriesForStore(String storeId) {
		Query query = Query.query(Criteria.where("storeId").is(storeId));
		return mongoTemplate.find(query, GroceryQuantity.class);
	}

	@Override
	public List<GroceryQuantity> getFinishedGroceryByStore(String storeId) {
		Query query = Query
				.query(Criteria.where("storeId").is(storeId).andOperator(Criteria.where("isFinished").is(true)));
		return mongoTemplate.find(query, GroceryQuantity.class);
	}
}
