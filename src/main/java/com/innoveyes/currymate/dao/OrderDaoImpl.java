/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.Order;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class OrderDaoImpl implements OrderDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void saveOrder(Order order) {
		mongoTemplate.save(order);
	}

	@Override
	public List<Order> findOrderByUserStoreAndDate(String userId, String storeId, Date orderedDate) {
		Query query = Query.query(Criteria.where("userId").is(userId).andOperator(Criteria.where("storeId").is(storeId))
				.andOperator(Criteria.where("orderedDate").is(orderedDate).andOperator(Criteria.where("isCancelled")
						.is(false).andOperator(Criteria.where("isPaymentDone").is(false)))));
		return mongoTemplate.find(query, Order.class);
	}

	@Override
	public List<Order> findOrderByUserStore(String userId, String storeId) {
		Query query = Query
				.query(Criteria.where("userId").is(userId).andOperator(Criteria.where("storeId").is(storeId)));
		return mongoTemplate.find(query, Order.class);
	}

	@Override
	public List<Order> findOrderByUserStoreAndCancelled(String userId, String storeId) {
		Query query = Query.query(Criteria.where("userId").is(userId).andOperator(Criteria.where("storeId").is(storeId))
				.andOperator(Criteria.where("isCancelled").is(true)));
		return mongoTemplate.find(query, Order.class);
	}

}
