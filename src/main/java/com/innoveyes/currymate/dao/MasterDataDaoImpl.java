/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.master.Area;
import com.innoveyes.currymate.model.master.Grocery;
import com.innoveyes.currymate.model.master.Item;
import com.innoveyes.currymate.model.master.Location;
import com.innoveyes.currymate.model.master.UserRole;
import com.innoveyes.currymate.model.master.Vegetable;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class MasterDataDaoImpl implements MasterDataDao{

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Area> allAreas() {
		
		return mongoTemplate.findAll(Area.class);
	}

	@Override
	public List<Grocery> allGroceries() {

		return mongoTemplate.findAll(Grocery.class);
	}

	@Override
	public List<Item> allItems() {

		return mongoTemplate.findAll(Item.class);
	}

	@Override
	public List<Location> allLocations() {

		return mongoTemplate.findAll(Location.class);
	}

	@Override
	public List<UserRole> allRoles() {

		return mongoTemplate.findAll(UserRole.class);
	}

	@Override
	public List<Vegetable> allVegetable() {

		return mongoTemplate.findAll(Vegetable.class);
	}

	@Override
	public void addArea(Area area) {
		mongoTemplate.save(area);
	}

	@Override
	public void addGrocery(Grocery grocery) {
		mongoTemplate.save(grocery);
	}

	@Override
	public void addItem(Item item) {
		mongoTemplate.save(item);
	}

	@Override
	public void addLocation(Location location) {
		mongoTemplate.save(location);
	}

	@Override
	public void addUserRole(UserRole role) {
		mongoTemplate.save(role);
	}

	@Override
	public void addvegetable(Vegetable vegetable) {
		mongoTemplate.save(vegetable);
	}

}
