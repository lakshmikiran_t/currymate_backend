/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.Favorite;
import com.innoveyes.currymate.model.Rating;

/**
 * @author Lakshmi Kiran
 *
 */
@Repository
@Transactional
public class RatingAndFavoriteDaoImpl implements RatingAndFavoriteDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void rateStore(Rating rating) {
		mongoTemplate.save(rating);
	}

	@Override
	public List<Rating> storeRating(String userProfileId, String storeId) {
		Query query = Query.query(
				Criteria.where("userProfileId").is(userProfileId).andOperator(Criteria.where("storeId").is(storeId)));
		return mongoTemplate.find(query, Rating.class);
	}

	@Override
	public void addFavorite(Favorite favorite) {
		mongoTemplate.save(favorite);
	}

	@Override
	public Favorite isFavorite(String userProfileId, String storeId) {
		Query query = Query.query(
				Criteria.where("userProfileId").is(userProfileId).andOperator(Criteria.where("storeId").is(storeId)));
		return mongoTemplate.findOne(query, Favorite.class);
	}

	@Override
	public void deleteFavorite(String userProfileId, String storeId) {
		Query query = Query.query(
				Criteria.where("userProfileId").is(userProfileId).andOperator(Criteria.where("storeId").is(storeId)));
		mongoTemplate.remove(query, Favorite.class);
	}

	@Override
	public List<Favorite> getFavoriteStores(String userProfileId) {
		Query query = Query.query(Criteria.where("userProfileId").is(userProfileId));
		return mongoTemplate.find(query, Favorite.class);
	}

}
