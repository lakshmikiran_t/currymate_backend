/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.PreparedItem;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class PreparedItemDaoImpl implements PreparedItemDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public void savePreparedItem(PreparedItem bean) {
		mongoTemplate.save(bean);
	}

	@Override
	public List<PreparedItem> getItemsByStore(String storeId) {
		Query query = Query.query(Criteria.where("storeId").is(storeId));
		return mongoTemplate.find(query, PreparedItem.class);
	}

	@Override
	public List<PreparedItem> getItemsByStoreAndDate(String storeId, Date createdDate) {
		Query query = Query.query(
				Criteria.where("storeId").is(storeId).andOperator(Criteria.where("createdDate").is(createdDate)));
		return mongoTemplate.find(query, PreparedItem.class);
	}

	@Override
	public PreparedItem findPreparedItemById(String preparedItemId) {
		Query query = Query.query(Criteria.where("preparedItemId").is(preparedItemId));
		return mongoTemplate.findOne(query, PreparedItem.class);
	}

}
