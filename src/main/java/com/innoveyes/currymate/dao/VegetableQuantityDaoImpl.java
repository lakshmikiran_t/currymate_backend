/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.VegetableQuantity;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class VegetableQuantityDaoImpl implements VegetableQuantityDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public VegetableQuantity getVegetableQuantityByStore(String vegetableId, String storeId) {
		Query query = Query
				.query(Criteria.where("vegetableId").is(vegetableId).andOperator(Criteria.where("storeId").is(storeId))
						.andOperator(Criteria.where("isFinished").is(false)));
		return mongoTemplate.findOne(query, VegetableQuantity.class);
	}

	@Override
	public VegetableQuantity saveVegetableQuantity(VegetableQuantity quantity) {
		mongoTemplate.save(quantity);
		return getVegetableQuantityByStore(quantity.getVegetableId(), quantity.getStoreId());
	}

	@Override
	public List<VegetableQuantity> getVegetablesForStore(String storeId) {
		Query query = Query.query(Criteria.where("storeId").is(storeId));
		return mongoTemplate.find(query, VegetableQuantity.class);
	}

	@Override
	public List<VegetableQuantity> getFinishedVegQuantityForStore(String storeId) {
		Query query = Query
				.query(Criteria.where("storeId").is(storeId).andOperator(Criteria.where("isFinished").is(true)));
		return mongoTemplate.find(query, VegetableQuantity.class);
	}
}
