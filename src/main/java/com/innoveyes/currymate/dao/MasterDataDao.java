/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import com.innoveyes.currymate.model.master.Area;
import com.innoveyes.currymate.model.master.Grocery;
import com.innoveyes.currymate.model.master.Item;
import com.innoveyes.currymate.model.master.Location;
import com.innoveyes.currymate.model.master.UserRole;
import com.innoveyes.currymate.model.master.Vegetable;

/**
 * @author LakshmiKiran
 *
 */
public interface MasterDataDao {
	
	public List<Area> allAreas();
	
	public List<Grocery> allGroceries();
	
	public List<Item> allItems();
	
	public List<Location> allLocations();
	
	public List<UserRole> allRoles();
	
	public List<Vegetable> allVegetable();
	
	public void addArea(Area area);
	
	public void addGrocery(Grocery grocery);
	
	public void addItem(Item item);
	
	public void addLocation(Location location);
	
	public void addUserRole(UserRole role);
	
	public void addvegetable(Vegetable vegetable);
}
