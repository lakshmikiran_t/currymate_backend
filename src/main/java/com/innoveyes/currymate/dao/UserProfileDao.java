/**
 * 
 */
package com.innoveyes.currymate.dao;

import com.innoveyes.currymate.model.UserLoginStatus;
import com.innoveyes.currymate.model.UserProfile;

/**
 * @author LakshmiKiran
 *
 */
public interface UserProfileDao {
	
	public UserProfile findUserById(String userId);
	
	public UserProfile findUserByMobile(String mobile);
	
	public UserLoginStatus isUserLoggedIn(String userId);
	
	public UserProfile signUp(UserProfile bean);
	
	public void updateUser(UserProfile bean);
	
	public void saveUserLoginStatus(UserLoginStatus status);
	
	public UserLoginStatus findUserLoginStatus(String sessionToken);
	
	public UserLoginStatus findLoginStatusById(String userId);
	
	
}
