/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import com.innoveyes.currymate.model.VegetableQuantity;

/**
 * @author LakshmiKiran
 *
 */
public interface VegetableQuantityDao {

	public VegetableQuantity getVegetableQuantityByStore(String vegetableId, String storeId);

	public VegetableQuantity saveVegetableQuantity(VegetableQuantity quantity);
	
	public List<VegetableQuantity> getVegetablesForStore(String storeId);
	
	public List<VegetableQuantity> getFinishedVegQuantityForStore(String storeId);
}
