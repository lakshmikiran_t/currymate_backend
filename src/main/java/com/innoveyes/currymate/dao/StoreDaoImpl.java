/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.innoveyes.currymate.model.Store;

/**
 * @author LakshmiKiran
 *
 */
@Repository
@Transactional
public class StoreDaoImpl implements StoreDao {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public Store saveStore(Store bean) {
		mongoTemplate.save(bean);
		return findStoreByPhone(bean.getStorePhone());
	}


	@Override
	public List<Store> findStoresByLocation(String locationId) {
		Query query = Query.query(Criteria.where("locationId").is(locationId));
		return mongoTemplate.find(query, Store.class);
	}

	@Override
	public List<Store> findStoresByArea(String areaId) {
		Query query = Query.query(Criteria.where("areaId").is(areaId));
		return mongoTemplate.find(query, Store.class);
	}

	@Override
	public List<Store> findStoresForUser(String userId) {
		Query query = Query.query(Criteria.where("userId").is(userId));
		return mongoTemplate.find(query, Store.class);
	}


	@Override
	public Store findStoreByPhone(String storePhone) {
		Query query = Query.query(Criteria.where("storePhone").is(storePhone));
		return mongoTemplate.findOne(query, Store.class);
	}


	@Override
	public Store findStoreById(String storeId) {
		Query query = Query.query(Criteria.where("storeId").is(storeId));
		return mongoTemplate.findOne(query, Store.class);
	}

}
