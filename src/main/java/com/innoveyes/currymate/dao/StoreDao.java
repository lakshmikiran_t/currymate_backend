/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import com.innoveyes.currymate.model.Store;

/**
 * @author LakshmiKiran
 *
 */
public interface StoreDao {
	
	public Store saveStore(Store bean);
	
	public List<Store> findStoresByLocation(String locationId);
	
	public List<Store> findStoresByArea(String areaId);
	
	public List<Store> findStoresForUser(String userId);
	
	public Store findStoreByPhone(String storePhone);
	
	public Store findStoreById(String storeId);
}
