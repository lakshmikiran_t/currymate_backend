/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.List;

import com.innoveyes.currymate.model.GroceryQuantity;

/**
 * @author LakshmiKiran
 *
 */
public interface GroceryQuantityDao {

	public GroceryQuantity getGroceryQuantityByStore(String groceryId, String storeId);
	
	public GroceryQuantity saveGroceryQuantity(GroceryQuantity quantity);
	
	public List<GroceryQuantity> getGroceriesForStore(String storeId);
	
	public List<GroceryQuantity> getFinishedGroceryByStore(String storeId);
}
