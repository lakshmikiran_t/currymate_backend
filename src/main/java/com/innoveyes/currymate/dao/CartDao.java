/**
 * 
 */
package com.innoveyes.currymate.dao;

import java.util.Date;
import java.util.List;

import com.innoveyes.currymate.model.Cart;

/**
 * @author LakshmiKiran
 *
 */
public interface CartDao {

	public void saveCart(Cart cart);

	public List<Cart> findTodayCartsByUserAndStore(String userId, String storeId, Date cartedDate);
	
	public List<Cart> findAllCartsByUserAndStore(String userId, String storeId);
}
