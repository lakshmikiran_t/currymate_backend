/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class UserLoginStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3491054075213370742L;
	
	@Id
	private String userLoginStatusId;
	private String userId;
	private String userRoleId;
	private String sessionToken;
	private Date lastLoggedIn;
	private Boolean isLoggedOut;
	private Date createdDate;
	private Date updatedDate;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getUserLoginStatusId() {
		return userLoginStatusId;
	}
	public void setUserLoginStatusId(String userLoginStatusId) {
		this.userLoginStatusId = userLoginStatusId;
	}
	public String getSessionToken() {
		return sessionToken;
	}
	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}
	public Date getLastLoggedIn() {
		return lastLoggedIn;
	}
	public void setLastLoggedIn(Date lastLoggedIn) {
		this.lastLoggedIn = lastLoggedIn;
	}
	public Boolean getIsLoggedOut() {
		return isLoggedOut;
	}
	public void setIsLoggedOut(Boolean isLoggedOut) {
		this.isLoggedOut = isLoggedOut;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "UserLoginStatus [userLoginStatusId=" + userLoginStatusId + ", userId=" + userId + ", userRoleId="
				+ userRoleId + ", sessionToken=" + sessionToken + ", lastLoggedIn=" + lastLoggedIn + ", isLoggedOut="
				+ isLoggedOut + ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + "]";
	}
}
