/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Cart implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2432992370239889587L;
	
	@Id
	private String cartId;
	private List<Order> orders;
	private Integer checkOutCost;
	private Boolean isPaid;
	private Date cartedDate;
	
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public Integer getCheckOutCost() {
		int totalCost = 0;
		for(Order order : orders) {
			totalCost = totalCost * order.getOrderCost();
		}
		return totalCost;
	}
	public void setCheckOutCost(Integer checkOutCost) {
		this.checkOutCost = checkOutCost;
	}
	public Boolean getIsPaid() {
		return isPaid;
	}
	public void setIsPaid(Boolean isPaid) {
		this.isPaid = isPaid;
	}
	public Date getCartedDate() {
		return cartedDate;
	}
	public void setCartedDate(Date cartedDate) {
		this.cartedDate = cartedDate;
	}
	@Override
	public String toString() {
		return "Cart [cartId=" + cartId + ", orders=" + orders + ", checkOutCost=" + checkOutCost + ", isPaid=" + isPaid
				+ ", cartedDate=" + cartedDate + "]";
	}
	
	
	
	
}
