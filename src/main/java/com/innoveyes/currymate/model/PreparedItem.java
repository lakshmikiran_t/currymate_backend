/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.innoveyes.currymate.bean.GroceryBean;
import com.innoveyes.currymate.bean.VegetableBean;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class PreparedItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -170198266855606556L;

	@Id
	private String preparedItemId;
	private String preparedItemName;
	private String itemId;
	private String itemType;
	private String storeId;
	private String itemImage;
	private List<GroceryBean> groceries;
	private List<VegetableBean> vegetables;
	private Integer totalGroceryCost;
	private Integer totalVegetableCost;
	private Integer totalQuantity;
	private Integer additionalCharges;
	private Integer totalPacks;
	private Integer orderedPacks;
	private Integer remainingPacks;
	private Integer costPerPack;
	private Integer packsPerKG;
	private Integer totalCost;
	private Boolean isFinished;
	private Date preparedDate;
	private Date updatedDate;

	public Integer getTotalGroceryCost() {
		return totalGroceryCost;
	}

	public void setTotalGroceryCost(Integer totalGroceryCost) {
		this.totalGroceryCost = totalGroceryCost;
	}

	public Integer getTotalVegetableCost() {
		return totalVegetableCost;
	}

	public void setTotalVegetableCost(Integer totalVegetableCost) {
		this.totalVegetableCost = totalVegetableCost;
	}

	public Integer getTotalCost() {
		return totalCost;
	}

	public void setTotalCost() {
		this.totalCost = this.totalGroceryCost + this.totalVegetableCost + this.additionalCharges;
	}

	public Integer getPacksPerKG() {
		return packsPerKG;
	}

	public void setPacksPerKG(Integer packsPerKG) {
		this.packsPerKG = packsPerKG;
	}

	public Integer getCostPerPack() {
		return costPerPack;
	}

	public void setCostPerPack(Integer costPerPack) {
		this.costPerPack = costPerPack;
	}

	public Integer getAdditionalCharges() {
		return additionalCharges;
	}

	public void setAdditionalCharges(Integer additionalCharges) {
		this.additionalCharges = additionalCharges;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getTotalPacks() {
		return totalPacks;
	}

	public void setTotalPacks(Integer totalPacks) {
		this.totalPacks = totalPacks;
	}

	public Integer getOrderedPacks() {
		return orderedPacks;
	}

	public void setOrderedPacks(Integer orderedPacks) {
		this.orderedPacks = orderedPacks;
	}

	public Integer getRemainingPacks() {
		return remainingPacks;
	}

	public void setRemainingPacks(Integer remainingPacks) {
		this.remainingPacks = remainingPacks;
	}

	public Boolean getIsFinished() {
		return isFinished;
	}

	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemImage() {
		return itemImage;
	}

	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getPreparedItemId() {
		return preparedItemId;
	}

	public void setPreparedItemId(String preparedItemId) {
		this.preparedItemId = preparedItemId;
	}

	public String getPreparedItemName() {
		return preparedItemName;
	}

	public void setPreparedItemName(String preparedItemName) {
		this.preparedItemName = preparedItemName;
	}

	public List<GroceryBean> getGroceries() {
		return groceries;
	}

	public void setGroceries(List<GroceryBean> groceries) {
		this.groceries = groceries;
	}

	public List<VegetableBean> getVegetables() {
		return vegetables;
	}

	public void setVegetables(List<VegetableBean> vegetables) {
		this.vegetables = vegetables;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getPreparedDate() {
		return preparedDate;
	}

	public void setPreparedDate(Date createdDate) {
		this.preparedDate = createdDate;
	}

	@Override
	public String toString() {
		return "PreparedItem [preparedItemId=" + preparedItemId + ", preparedItemName=" + preparedItemName + ", itemId="
				+ itemId + ", itemType=" + itemType + ", storeId=" + storeId + ", itemImage=" + itemImage
				+ ", groceries=" + groceries + ", vegetables=" + vegetables + ", totalGroceryCost=" + totalGroceryCost
				+ ", totalVegetableCost=" + totalVegetableCost + ", totalQuantity=" + totalQuantity
				+ ", additionalCharges=" + additionalCharges + ", totalPacks=" + totalPacks + ", orderedPacks="
				+ orderedPacks + ", remainingPacks=" + remainingPacks + ", costPerPack=" + costPerPack + ", packsPerKG="
				+ packsPerKG + ", totalCost=" + totalCost + ", isFinished=" + isFinished + ", preparedDate="
				+ preparedDate + ", updatedDate=" + updatedDate + "]";
	}

}
