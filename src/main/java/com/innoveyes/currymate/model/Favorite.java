/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Lakshmi Kiran
 *
 */
@Document
public class Favorite implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7777624026881195996L;
	
	@Id
	private String favoriteId;
	private String userProfileId;
	private String storeId;
	private Date createdDate;
	
	public String getFavoriteId() {
		return favoriteId;
	}
	public void setFavoriteId(String favoriteId) {
		this.favoriteId = favoriteId;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userId) {
		this.userProfileId = userId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "Favorite [favoriteId=" + favoriteId + ", userId=" + userProfileId + ", storeId=" + storeId + ", createdDate="
				+ createdDate + "]";
	}
	
	

}
