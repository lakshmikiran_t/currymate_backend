/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Lakshmi Kiran
 *
 */
@Document
public class Rating implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3485108176302099124L;
	
	@Id
	private String ratingId;
	private String userProfileId;
	private String storeId;
	private Integer rating;
	private Date createdDate;
	
	public String getRatingId() {
		return ratingId;
	}
	public void setRatingId(String ratingId) {
		this.ratingId = ratingId;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Override
	public String toString() {
		return "Rating [ratingId=" + ratingId + ", userProfileId=" + userProfileId + ", storeId=" + storeId
				+ ", rating=" + rating + ", createdDate=" + createdDate + "]";
	}
	
	
}
