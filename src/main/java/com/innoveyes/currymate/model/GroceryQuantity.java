/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class GroceryQuantity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5301666178354950341L;
	
	@Id
	private String groceryQuantityId;
	private String storeId;
	private String groceryId;
	private String groceryName;
	private Integer costPerKG;
	private Integer totalQuantity;
	private Integer usedQuantity;
	private Integer remainingQuantity;
	private Integer totalGroceryCost;
	private Boolean isFinished = false;
	private Date createdDate;
	private Date updatedDate;
	
	
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getGroceryQuantityId() {
		return groceryQuantityId;
	}
	public void setGroceryQuantityId(String groceryQuantityId) {
		this.groceryQuantityId = groceryQuantityId;
	}
	public String getGroceryId() {
		return groceryId;
	}
	public void setGroceryId(String groceryId) {
		this.groceryId = groceryId;
	}
	public String getGroceryName() {
		return groceryName;
	}
	public void setGroceryName(String groceryName) {
		this.groceryName = groceryName;
	}
	public Integer getCostPerKG() {
		return costPerKG;
	}
	public void setCostPerKG(Integer costPerKG) {
		this.costPerKG = costPerKG;
	}
	public Integer getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public Integer getUsedQuantity() {
		return usedQuantity;
	}
	public void setUsedQuantity(Integer usedQuantity) {
		this.usedQuantity = usedQuantity;
	}
	public Integer getRemainingQuantity() {
		return remainingQuantity;
	}
	public void setRemainingQuantity(Integer remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}
	public Integer getTotalGroceryCost() {
		return totalGroceryCost;
	}
	public void setTotalGroceryCost(Integer totalGroceryCost) {
		this.totalGroceryCost = totalGroceryCost;
	}
	public Boolean getIsFinished() {
		return isFinished;
	}
	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "GroceryQuantity [groceryQuantityId=" + groceryQuantityId + ", storeId=" + storeId + ", groceryId="
				+ groceryId + ", groceryName=" + groceryName + ", costPerKG=" + costPerKG + ", totalQuantity="
				+ totalQuantity + ", usedQuantity=" + usedQuantity + ", remainingQuantity=" + remainingQuantity
				+ ", totalGroceryCost=" + totalGroceryCost + ", isFinished=" + isFinished + ", createdDate="
				+ createdDate + ", updatedDate=" + updatedDate + "]";
	}
	
	
	
	

}
