/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Order implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2263046911545199974L;
	
	@Id
	private String orderId;
	private String preparedItemId;
	private String storeId;
	private String userProfileId;
	private Integer noOfPacks;
	private Integer orderCost;
	private Integer itemCost;
	private Boolean isCancelled;
	private Boolean isPaymentDone;
	private Date orderedDate;
	
	
	public Boolean getIsPaymentDone() {
		return isPaymentDone;
	}
	public void setIsPaymentDone(Boolean isPaymentDone) {
		this.isPaymentDone = isPaymentDone;
	}
	public Boolean getIsCancelled() {
		return isCancelled;
	}
	public void setIsCancelled(Boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getPreparedItemId() {
		return preparedItemId;
	}
	public void setPreparedItemId(String preparedItemId) {
		this.preparedItemId = preparedItemId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}
	public Date getOrderedDate() {
		return orderedDate;
	}
	public void setOrderedDate(Date orderedDate) {
		this.orderedDate = orderedDate;
	}
	public Integer getNoOfPacks() {
		return noOfPacks;
	}
	public void setNoOfPacks(Integer noOfPacks) {
		this.noOfPacks = noOfPacks;
	}
	public Integer getOrderCost() {
		return orderCost;
	}
	public void setOrderCost(Integer orderCost) {
		this.orderCost = orderCost;
	}
	public Integer getItemCost() {
		return itemCost;
	}
	public void setItemCost(Integer itemCost) {
		this.itemCost = itemCost;
	}
	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", preparedItemId=" + preparedItemId + ", storeId=" + storeId
				+ ", userProfileId=" + userProfileId + ", noOfPacks=" + noOfPacks + ", orderCost=" + orderCost
				+ ", itemCost=" + itemCost + ", isCancelled=" + isCancelled + ", isPaymentDone=" + isPaymentDone
				+ ", orderedDate=" + orderedDate + "]";
	}
}
