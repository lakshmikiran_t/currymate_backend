/**
 * 
 */
package com.innoveyes.currymate.model.master;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Grocery implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4292941083393277799L;
	
	@Id
	private String groceryId;
	@Indexed(unique=true)
	private String groceryName;
	
	public String getGroceryId() {
		return groceryId;
	}
	public void setGroceryId(String groceryId) {
		this.groceryId = groceryId;
	}
	public String getGroceryName() {
		return groceryName;
	}
	public void setGroceryName(String groceryName) {
		this.groceryName = groceryName;
	}

	public Grocery() {
		
	}
	public Grocery(String groceryName) {
		super();
		this.groceryName = groceryName;
	}
	
}
