/**
 * 
 */
package com.innoveyes.currymate.model.master;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class UserRole implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 521228867729060173L;
	
	@Id
	private String userRoleId;
	@Indexed(unique=true)
	private String userRoleName;
	
	public String getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getUserRoleName() {
		return userRoleName;
	}
	public void setUserRoleName(String userRoleName) {
		this.userRoleName = userRoleName;
	}

	public UserRole() {
		
	}
	public UserRole(String userRoleName) {
		this.userRoleName = userRoleName;
	}
	

}
