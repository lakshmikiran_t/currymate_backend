/**
 * 
 */
package com.innoveyes.currymate.model.master;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Area implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6573797901416373629L;
	
	@Id
	private String areaId;
	@Indexed(unique=true)
	private String areaName;
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	
	public Area() {

	}
	public Area(String areaName) {
		this.areaName = areaName;
	}
	
	
}
