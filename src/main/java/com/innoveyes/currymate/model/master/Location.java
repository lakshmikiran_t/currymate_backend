/**
 * 
 */
package com.innoveyes.currymate.model.master;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Location implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6332302008518406151L;
	
	@Id
	private String locationId;
	@Indexed(unique=true)
	private String locationName;
	
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Location() {
		
	}
	public Location(String locationName) {
		this.locationName = locationName;
	}
	
}
