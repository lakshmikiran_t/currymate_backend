/**
 * 
 */
package com.innoveyes.currymate.model.master;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Item implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3715199822340051210L;
	@Id
	private String itemId;
	@Indexed(unique=true)
	private String itemName;
	private String itemType;
	private String itemImage;
	
	
	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemType=" + itemType + ", itemImage="
				+ itemImage + "]";
	}
	public String getItemImage() {
		return itemImage;
	}
	public void setItemImage(String itemImage) {
		this.itemImage = itemImage;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public Item() {
		
	}
	public Item(String itemName, String itemType) {
		this.itemName = itemName;
		this.itemType = itemType;
	}
	
}
