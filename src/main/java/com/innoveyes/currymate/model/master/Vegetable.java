/**
 * 
 */
package com.innoveyes.currymate.model.master;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Vegetable {
	
	@Id
	private String vegetableId;
	@Indexed(unique=true)
	private String vegetableName;
	
	public String getVegetableId() {
		return vegetableId;
	}
	public void setVegetableId(String vegetableId) {
		this.vegetableId = vegetableId;
	}
	public String getVegetableName() {
		return vegetableName;
	}
	public void setVegetableName(String vegetableName) {
		this.vegetableName = vegetableName;
	}

	public Vegetable() {
		
	}
	public Vegetable(String vegetableName) {
		this.vegetableName = vegetableName;
	}
	
}
