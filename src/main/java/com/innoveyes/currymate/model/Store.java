/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class Store implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9127039463776948099L;
	
	@Id
	private String stroreId;
	private String storeName;
	private String userProfileId;
	private String locationId;
	private String areaId;
	private String storeAddress;
	@Indexed(unique=true)
	private String storePhone;
	private String pincode;
	private String[] storeImages;
	private Integer rating;
	private Date createdDate;
	private Date updatedDate;
	
	
	
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String[] getStoreImages() {
		return storeImages;
	}
	public void setStoreImages(String[] storeImages) {
		this.storeImages = storeImages;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getStroreId() {
		return stroreId;
	}
	public void setStroreId(String stroreId) {
		this.stroreId = stroreId;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	public String getAreaId() {
		return areaId;
	}
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(String storeAddress) {
		this.storeAddress = storeAddress;
	}
	public String getStorePhone() {
		return storePhone;
	}
	public void setStorePhone(String storePhone) {
		this.storePhone = storePhone;
	}
	public String getUserProfileId() {
		return userProfileId;
	}
	public void setUserProfileId(String userProfileId) {
		this.userProfileId = userProfileId;
	}
	@Override
	public String toString() {
		return "Store [stroreId=" + stroreId + ", storeName=" + storeName + ", userProfileId=" + userProfileId
				+ ", locationId=" + locationId + ", areaId=" + areaId + ", storeAddress=" + storeAddress
				+ ", storePhone=" + storePhone + ", pincode=" + pincode + ", storeImages="
				+ Arrays.toString(storeImages) + ", rating=" + rating + ", createdDate=" + createdDate
				+ ", updatedDate=" + updatedDate + "]";
	}
}
