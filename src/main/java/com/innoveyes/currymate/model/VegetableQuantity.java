/**
 * 
 */
package com.innoveyes.currymate.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author LakshmiKiran
 *
 */
@Document
public class VegetableQuantity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5756957545033099277L;
	
	@Id
	private String vegetableQuantityId;
	private String storeId;
	private String vegetableId;
	private String vegetableName;
	private Integer costPerKG;
	private Integer totalQuantity;
	private Integer usedQuantity;
	private Integer remainingQuantity;
	private Integer totalVegetableCost;
	private Boolean isFinished = false;
	private Date createdDate;
	private Date updatedDate;
	
	
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getVegetableQuantityId() {
		return vegetableQuantityId;
	}
	public void setVegetableQuantityId(String vegetableQuantityId) {
		this.vegetableQuantityId = vegetableQuantityId;
	}
	public String getVegetableId() {
		return vegetableId;
	}
	public void setVegetableId(String vegetableId) {
		this.vegetableId = vegetableId;
	}
	public String getVegetableName() {
		return vegetableName;
	}
	public void setVegetableName(String vegetableName) {
		this.vegetableName = vegetableName;
	}
	public Integer getCostPerKG() {
		return costPerKG;
	}
	public void setCostPerKG(Integer costPerKG) {
		this.costPerKG = costPerKG;
	}
	public Integer getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public Integer getUsedQuantity() {
		return usedQuantity;
	}
	public void setUsedQuantity(Integer usedQuantity) {
		this.usedQuantity = usedQuantity;
	}
	public Integer getRemainingQuantity() {
		return remainingQuantity;
	}
	public void setRemainingQuantity(Integer remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}
	public Integer getTotalVegetableCost() {
		return totalVegetableCost;
	}
	public void setTotalVegetableCost(Integer totalVegetableCost) {
		this.totalVegetableCost = totalVegetableCost;
	}
	public Boolean getIsFinished() {
		return isFinished;
	}
	public void setIsFinished(Boolean isFinished) {
		this.isFinished = isFinished;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Override
	public String toString() {
		return "VegetableQuantity [vegetableQuantityId=" + vegetableQuantityId + ", storeId=" + storeId
				+ ", vegetableId=" + vegetableId + ", vegetableName=" + vegetableName + ", costPerKG=" + costPerKG
				+ ", totalQuantity=" + totalQuantity + ", usedQuantity=" + usedQuantity + ", remainingQuantity="
				+ remainingQuantity + ", totalVegetableCost=" + totalVegetableCost + ", isFinished=" + isFinished
				+ ", createdDate=" + createdDate + ", updatedDate=" + updatedDate + "]";
	}

	
	
}
