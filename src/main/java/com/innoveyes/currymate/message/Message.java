/**
 * 
 */
package com.innoveyes.currymate.message;

/**
 * @author LakshmiKiran
 *
 */
public enum Message {
	
	SUCCESS("success"),
	FAILURE("failure"),
	STATUS("status"),
	MESSAGE("message"),
	DATA("data"),
	STATUS_CODE("status_code"),
	SECRET_KEY("F21E2A7FB6C68037FAEAA55222E320F7"),
	CHAR_LIST("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"),
	ROLE_USER ("5a16887004d3e503570cf2b9"),
	ROLE_ADMIN ("5a16887004d3e503570cf2ba"),
	ROLE_SUPER_ADMIN ("5a16887004d3e503570cf2bb");
	
	private String value;

	Message(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
