/**
 * 
 */
package com.innoveyes.currymate.message;

/**
 * @author LakshmiKiran
 *
 */
public interface MessageConstants {
	
	public static final String SUCCESS = "success";
	
	public static final String FAILURE = "failure";
	
	public static final String STATUS = "status";
	
	public static final String MESSAGE = "message";
	
	public static final String DATA = "data";
	
	public static final String STATUS_CODE = "status_code";
}
